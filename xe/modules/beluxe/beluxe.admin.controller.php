<?php

	class beluxeAdminController extends beluxe
	{

	/**************************************************************/
	/*********** @initialization						***********/

		function init()
		{
		}

	/**************************************************************/
	/*********** @private function					***********/

		function _setLocation($module_srl, $act)
		{
			if(!in_array(Context::getRequestMethod(),array('XMLRPC','JSON')))
			{
				$returnUrl = Context::get('success_return_url');

				if(!$returnUrl)
				{
					$module = Context::get('module');
					if($module!='admin') $module = '';

					$returnUrl = getNotEncodedUrl(
						'',
						$module?'module':'mid', $module?$module:Context::get('mid'),
						'module_srl', $module_srl ? $module_srl : '',
						'act', $act
					);
				}

				if(!$returnUrl) $returnUrl = Context::get('error_return_url');

				header('location:'.$returnUrl);
			}

			return;
		}

		function _setModuleInfo($module_srl)
		{
			$info_list = func_get_args();
			if(!$module_srl || count($info_list) < 3) return;

			array_shift($info_list);

			$oModuleModel = &getModel('module');
			$oModuleController = &getController('module');

			$args = $oModuleModel->getModuleInfoByModuleSrl($module_srl);

			for($i=0, $cnt=count($info_list); $i<$cnt; $i+=2)
			{
				$args->{$info_list[$i]} = $info_list[$i+1];
			}

			$oModuleController->updateModule($args);
		}

		/* @brief Delete a category */
		function doDeleteCategory($category_srl)
		{
			$oThisModel = &getModel(__XEFM_NAME__);

			$oDocumentModel = &getModel('document');
			$category_info = $oDocumentModel->getCategory($category_srl, array('module_srl'));
			if(!$category_info->module_srl) return new Object(-1, 'msg_invalid_request');

			// Display an error that the category cannot be deleted if it has a child
			$args->category_srl = $category_srl;
			$output = executeQuery('document.getChildCategoryCount', $args);
			if(!$output->toBool()) return $output;
			if($output->data->count>0) return new Object(-1, 'msg_cannot_delete_for_child');

			$target_category_srl = 0;

			// Update category_srl of the documents in the same category to 0
			unset($args);
			$args->target_category_srl = $target_category_srl;
			$args->source_category_srl = $category_srl;
			$output = executeQuery('document.updateDocumentCategory', $args);
			if(!$output->toBool()) return $output;

			// Update item count
			unset($args);
			$args->module_srl = $category_info->module_srl;
			$args->category_srl = $target_category_srl;
			$output = executeQuery('document.getDocumentCount', $args);

			$args->document_count = (int)$output->data->count;
			$output = executeQuery('document.updateCategoryCount', $args);
			if(!$output->toBool()) return $output;

			// Delete a category information
			unset($args);
			$args->category_srl = $category_srl;
			return executeQuery('document.deleteCategory', $args);
		}

		/* @brief Add a category */
		function doInsertCategory($obj)
		{
			// Sort the order to display if a child category is added
			if($obj->parent_srl)
			{
				// Get its parent category
				$oDocumentModel = &getModel('document');
				$parent = $oDocumentModel->getCategory($obj->parent_srl, array('module_srl','category_srl','list_order'));
				if($obj->parent_srl!=$parent->category_srl) return new Object(-1, 'msg_invalid_request');

				/* list_order 는 이미 입력되 있기에 불필요
				$obj->list_order = $parent->list_order;
				$this->doUpdateCategoryListOrder($parent->module_srl, $parent->list_order + 1);
				*/
			}
			else
			{
				//$obj->list_order = $obj->category_srl = getNextSequence();
			}

			if(!$obj->category_srl) $obj->category_srl = getNextSequence();
			$output = executeQuery('document.insertCategory', $obj);
			if($output->toBool()) $output->add('category_srl', $obj->category_srl);

			return $output;
		}

		/* @brief Update category information */
		function doUpdateCategory($obj)
		{
			return executeQuery('document.updateCategory', $obj);
		}

		/* @brief Increase list_count from a specific category
		function doUpdateCategoryListOrder($module_srl, $list_order)
		{
			$args->module_srl = $module_srl;
			$args->list_order = $list_order;
			return executeQuery('document.updateCategoryOrder', $args);
		}
		 */

	/**************************************************************/
	/*********** @public function					  ***********/

		/* @brief Create a new beluxe */
		function procBeluxeAdminInsert()
		{
			$args = Context::getRequestVars();
			$args->module = __XEFM_NAME__;
			$args->mid = $args->module_mid;

			if(!$args->mid) return new Object(-1, 'msg_invalid_request');

			// 저장에 필요없는 값 지움
			unset($args->module_mid);
			unset($args->_filter);
			unset($args->error_return_url);
			unset($args->ruleset);

			$order = explode(',', __XEFM_ORDER__);

			// 필요한 변수 값 체크
			if(!$args->site_srl) $args->site_srl = 0;
			if(!$args->browser_title) $args->browser_title = $args->mid;
			if(!$args->skin) $args->skin = 'default';

			if($args->use_anonymous!='Y') $args->use_anonymous= 'N';
			if($args->consultation!='Y') $args->consultation = 'N';
			if(!in_array($args->order_target,$order)) $args->order_target = 'list_order';
			if(!in_array($args->order_type,array('asc','desc'))) $args->order_type = 'asc';

			if(count($args->use_status)) $args->use_status = implode(',', $args->use_status);
			if(count($args->use_c_status)) $args->use_c_status = implode(',', $args->use_c_status);

			// 9 개만 받고 빈것들 제거
			$custom_status = explode(',', $args->custom_status);
			$args->custom_status = array();
			foreach($custom_status as $val)
			{
				$val = trim($val);
				if(!$val) continue;
				$args->custom_status[] = $val;
				if(count($args->custom_status)>8) break;
			}
			$args->custom_status = implode(',',$args->custom_status);

			// ...로드
			$oModuleController = &getController('module');
			$oModuleAdminController = &getAdminController('module');

			$oDB = &DB::getInstance();
			if($oDB)
			{
				$oDB->begin();

				if(!$args->module_srl)
				{
					// 모듈 등록
					$output = $oModuleController->insertModule($args);
					if(!$output->toBool())
					{
						$oDB->rollback();
						return $output;
					}

					$module_srl = $output->get('module_srl');

					$msg_code = 'success_registed';
				}
				else
				{
					$output = $oModuleController->updateModule($args);
					if(!$output->toBool()) return $output;

					$module_srl = $output->get('module_srl');

					$msg_code = 'success_updated';
				}

				$oDB->commit();
			}
			else return new Object(-1,'msg_dbconnect_failed');

			$this->add('page', Context::get('page'));
			$this->add('module_srl', $module_srl);

			$this->setMessage($msg_code);
			$this->_setLocation($module_srl, 'dispBeluxeAdminModuleInfo');
		}

		/* @brief Delete a beluxe */
		function procBeluxeAdminDelete()
		{
			$module_srl = Context::get('module_srl');
			if(!$module_srl) return new Object(-1, 'msg_invalid_request');

			$oDB = &DB::getInstance();
			if($oDB)
			{
				$oDB->begin();

				/*/ 삭제된 문서도 같이 삭제
				 * TODO extra_keys,declared_log, voted_log, readed_log, 휴지통 안지워짐 이슈 등록중...
				$args->module_srl = $module_srl.','.($module_srl*-1);
				$output = executeQuery(__XEFM_NAME__.'.deleteBeluxe', $args);
				if(!$output->toBool())
				{
					$oDB->rollback();
					return $output;
				}*/

				$oModuleController = &getController('module');
				$output = $oModuleController->deleteModule($module_srl);
				if(!$output->toBool())
				{
					$oDB->rollback();
					return $output;
				}

				$oDB->commit();

				//remove from cache
				/* TODO 자주 바껴서 캐시까지 쓸 필요없을것 같다.
				$oCacheHandler = &CacheHandler::getInstance('object');
				if($oCacheHandler->isSupport())
				{
					$oCacheHandler->invalidateGroupKey('documentBestList');
					$oCacheHandler->invalidateGroupKey('commentBestList_'.$module_srl);
				}
				*/

				if(is_dir(__XEFM_CACHE__)){
					// 캐시삭제
					$directory = dir(__XEFM_CACHE__);
					while($entry = $directory->read()) {
						if ($entry != '.' && $entry != '..' && !is_dir(__XEFM_CACHE__.$entry)) {
							if (preg_match ('/'.$module_srl.'\..+/', $entry)) @unlink(__XEFM_CACHE__.$entry);
						}
					}
					$directory->close();
				}

				if(is_dir(__XEDM_CACHE__)){
					// 캐시삭제
					$directory = dir(__XEDM_CACHE__);
					while($entry = $directory->read()) {
						if ($entry != '.' && $entry != '..' && !is_dir(__XEDM_CACHE__.$entry)) {
							if (preg_match ('/'.$module_srl.'\..+/', $entry)) @unlink(__XEDM_CACHE__.$entry);
						}
					}
					$directory->close();
				}
			}
			else return new Object(-1,'msg_dbconnect_failed');

			$this->add('page',Context::get('page'));

			$this->setMessage('success_deleted');
			$this->_setLocation('', 'dispBeluxeAdminList');
		}

		/* @brief Add a category */
		function procBeluxeAdminInsertCategory()
		{
			$module_srl = Context::get('module_srl');
			if(!$module_srl) return new Object(-1,'msg_invalid_request');

			$item_key = Context::get('item_key');
			$parent_key = Context::get('parent_key');
			$item_title = Context::get('item_title');
			$item_color = Context::get('item_color');
			$item_type = Context::get('item_type');
			$item_navi = Context::get('item_navi');
			$item_expand = Context::get('item_expand');
			$group_srls = Context::get('group_srls');

			$pinf = array();

			$oDB = &DB::getInstance();
			if($oDB)
			{
				$oDB->begin();

				foreach($item_key as $key=>$category_srl)
				{
					unset($args);
					$args->list_order = ($key + 1) * 100;

					// 임시키로  만들어진 메뉴의 실제키 검사를 위해 준비하고 부모가 있으면 실제키 가져오기
					$pinf[$category_srl] = $category_srl;
					$args->parent_srl = (int) $pinf[$parent_key[$key]];

					// 0 보다 작다면 새로운 메뉴 생성을 위해 null
					$args->category_srl = ($category_srl > 0)?$category_srl:null;

					$args->module_srl = $module_srl;
					$args->title = $item_title[$key]?$item_title[$key]:'Untitled';
					$args->color = $item_color[$key];
					$args->expand = ($item_expand[$key]!="Y")?'N':'Y';
					$args->group_srls = str_replace('|@|',',',$group_srls[$key]);

					// type,navi필드가 없으니 description필드와 같이 저장, navi 필드는 불필요 문자 제거, description필드 크기는 200 이므로 주의...
					$args->description = $item_type[$key].'|@|'.preg_replace('/[^0-9a-zA-Z]+,|,$/','',trim($item_navi[$key])).'|@|';

					$oDocumentModel = &getModel('document');

					// Check if already exists
					if($args->category_srl)
					{
						$category_info = $oDocumentModel->getCategory($args->category_srl, array('category_srl'));
						if($category_info->category_srl != $args->category_srl) $args->category_srl = null;
					}

					// Update if exists
					if($args->category_srl)
					{
						$output = $this->doUpdateCategory($args);
						if(!$output->toBool())
						{
							$oDB->rollback();
							return $output;
						}
						// Insert if not exist
					}
					else
					{
						$output = $this->doInsertCategory($args);
						if(!$output->toBool())
						{
							$oDB->rollback();
							return $output;
						}

						// 임시키로  만들어진 메뉴의 실제키 저장
						$pinf[$category_srl] = $output->get('category_srl');
					}

				}

				$oDB->commit();
			}
			else return new Object(-1,'msg_dbconnect_failed');

			// 기본 분류 제목,타입 저장
			$default_title = Context::get('category_default_title');
			$default_type = Context::get('category_default_type');
			$this->_setModuleInfo($module_srl, 'category_default_title', $default_title, 'category_default_type', $default_type);

			// 캐쉬 갱신
			require_once(__XEFM_PATH__.'classes.cache.php');
			beluxeCache::categoryList($args->module_srl, true);

			$this->add('xml_file', sprintf(__XEDM_CACHE__.'%s.xml.php', $args->module_srl));
			$this->add('module_srl', $args->module_srl);
			$this->add('category_srl', $args->category_srl);
			$this->add('parent_srl', $args->parent_srl);

			$this->setMessage('success_updated');
			$this->_setLocation($args->module_srl, 'dispBeluxeAdminCategoryInfo');
		}

		/* @brief Delete a category */
		function procBeluxeAdminDeleteCategory()
		{
			// List variables
			$args = Context::gets('module_srl','category_srl');
			$module_srl = $args->module_srl;
			if(!$module_srl) return new Object(-1,'msg_invalid_request');

			// Get original information
			$oDocumentModel = &getModel('document');
			$category_info = $oDocumentModel->getCategory($args->category_srl, array('parent_srl'));
			$add_category_srl = ($category_info->parent_srl)?$category_info->parent_srl:$args->category_srl;

			// Display an error that the category cannot be deleted if it has a child node
			$out_count = executeQuery('document.getChildCategoryCount',$args);
			if($out_count->data->count > 0) return new Object(-1, 'msg_cannot_delete_for_child');

			$oDB = &DB::getInstance();
			if($oDB)
			{
				$oDB->begin();

				// Remove from the DB
				$output = $this->doDeleteCategory($args->category_srl);
				if(!$output->toBool())
				{
					$oDB->rollback();
					return $output;
				}

				$oDB->commit();

				//remove from cache
				/* TODO 자주 바껴서 캐시까지 쓸 필요없을것 같다.
				$oCacheHandler = &CacheHandler::getInstance('object');
				if($oCacheHandler->isSupport())
				{
					$oCacheHandler->invalidateGroupKey('documentBestList');
					$oCacheHandler->invalidateGroupKey('commentBestList_'.$module_srl);
				}
				*/
			}
			else return new Object(-1,'msg_dbconnect_failed');

			require_once(__XEFM_PATH__.'classes.cache.php');
			beluxeCache::categoryList($module_srl, true);

			$this->add('xml_file', sprintf(__XEDM_CACHE__.'%s.xml.php', $module_srl));
			$this->add('category_srl', $add_category_srl);

			$this->setMessage('success_deleted');
		}

		/* @brief Create a xml file of category */
		function procBeluxeAdminMakeCategoryCache()
		{
			$module_srl = Context::get('module_srl');
			if(!$module_srl) return new Object(-1,'msg_invalid_request');

			require_once(__XEFM_PATH__.'classes.cache.php');
			beluxeCache::categoryList($module_srl, true);
		}

		function procBeluxeAdminColumnSetting()
		{
			$module_srl = Context::get('module_srl');
			if(!$module_srl) return new Object(-1,'msg_invalid_request');

			$column_key = Context::get('column_key');
			$column_option = Context::get('column_option');
			$column_color = Context::get('column_color');

			$list_arr = array();

			foreach($column_key as $key=>$val)
			{
				$option = explode('|@|', $column_option[$key]);
				$color = $column_color[$key];

				$list_arr[$val] = array($key + 1,($color!='transparent'?$color:''),$option[0],$option[1],$option[2]);
			}

			$oModuleController = &getController('module');
			$output = $oModuleController->insertModulePartConfig('beluxe', $module_srl, $list_arr);
			if(!$output->toBool()) return $output;

			require_once(__XEFM_PATH__.'classes.cache.php');
			beluxeCache::columnConfigList($module_srl);

			$this->setMessage('success_updated');
			$this->_setLocation($module_srl, 'dispBeluxeAdminColumnSetting');
		}

		/* @brief Create a cache of column config */
		function procBeluxeAdminMakeColumnCache()
		{
			$module_srl = Context::get('module_srl');
			if(!$module_srl) return new Object(-1,'msg_invalid_request');

			require_once(__XEFM_PATH__.'classes.cache.php');
			beluxeCache::columnConfigList($module_srl);
		}

		function procBeluxeAdminInsertExtraKey()
		{
			$module_srl = Context::get('module_srl');
			if(!$module_srl) return new Object(-1,'msg_invalid_request');

			$extra_eid = Context::get('extra_eid');
			$extra_idx = Context::get('extra_idx');
			$extra_name = Context::get('extra_name');
			$extra_default = Context::get('extra_default');
			$extra_desc = Context::get('extra_desc');
			$extra_type = Context::get('extra_type');
			$extra_option = Context::get('extra_option');

			$chk_eid = array();

			$oDB = &DB::getInstance();
			if($oDB)
			{
				$oDB->begin();

				foreach($extra_eid as $key=>$val)
				{
					$val = trim($val);
					if (!$val || preg_match('/^[^a-z]|[^a-z0-9_]+$/i', $val)) continue;

					$eid = $val;
					// 중복 안되게 체크
					if($chk_eid[$eid]) $eid = $eid.'_'.count($chk_eid);

					$var_idx = $key + 1;
					$idx = (int)$extra_idx[$key];
					$type = $extra_type[$key];
					$name = $extra_name[$key];
					$desc = $extra_desc[$key];
					$default = $extra_default[$key];
					$is_required = explode('|@|',$extra_option[$key]);
					$is_required = $is_required[0];

					unset($args);
					$args->module_srl = $module_srl;
					$args->var_default = $default;
					$args->var_desc = $desc;
					$args->var_name = $name?$name:$eid;
					$args->var_type = $type?$type:'text';
					$args->var_is_required = $is_required=='Y'?'Y':'N';
					$args->var_search = 'N';
					$ch_args->module_srl = $module_srl;

					// 바꾸기전  idx가 있으면  unique 한 값으로 변경
					$args->var_idx = $var_idx;
					$oExtraKeys = executeQuery('document.getDocumentExtraKeys', $args);
					if($oExtraKeys->data){
						$ch_args->var_idx = $var_idx;
						$ch_args->new_idx = $new_idx = (time() + $key) * -1;
						$output = executeQuery('document.updateDocumentExtraKeyIdx', $ch_args);
						if($output->toBool()) $output = executeQuery('document.updateDocumentExtraVarIdx', $ch_args);
						if(!$output->toBool()){
							$oDB->rollback();
							return $output;
						}
					}

					// 바꾸기전  eid로  idx 추출
					$args->eid = $eid;
					$oExtraKeys = executeQuery('beluxe.getExtraKeys', $args);
					// insert or update
					if(!$oExtraKeys->data){
						$output = executeQuery('document.insertDocumentExtraKey', $args);
					}else{
						// 업데이트
						$args->var_idx = $var_idx;
						$output = executeQuery('beluxe.updateExtraKey', $args);
						if($output->toBool()){
							// Vars 값의 idx 새로 저장
							$ch_args->var_idx = $oExtraKeys->data->idx;
							$ch_args->new_idx = $var_idx;
							$output = executeQuery('document.updateDocumentExtraVarIdx', $ch_args);
						}
					}

					if(!$output->toBool()){
						$oDB->rollback();
						return $output;
					}

					// 중복 체크
					$chk_eid[$eid] = true;
				}

				$oDB->commit();
			}
			else return new Object(-1,'msg_dbconnect_failed');

			require_once(__XEFM_PATH__.'classes.cache.php');
			beluxeCache::columnConfigList($module_srl);

			$this->setMessage('success_updated');
			$this->_setLocation($module_srl, 'dispBeluxeAdminExtraKeys');
		}

		function procBeluxeAdminDeleteExtraKey()
		{
			// List variables
			$args = Context::gets('module_srl','extra_idx');
			$module_srl = $args->module_srl;
			$var_idx = $args->extra_idx;
			if(!$module_srl || !$var_idx) return new Object(-1,'msg_invalid_request');

			$oDocumentController = &getController('document');
			$output = $oDocumentController->deleteDocumentExtraKeys($module_srl, $var_idx);
			if(!$output->toBool()) return $output;

			require_once(__XEFM_PATH__.'classes.cache.php');
			beluxeCache::columnConfigList($module_srl);

			$this->setMessage('success_deleted');
		}

	/**************************************************************/

	}

?>
