<?php

	require_once(_XE_PATH_.'modules/beluxe/beluxe.view.php');

	class beluxeMobile extends beluxeView
	{
		function init()
		{
			// module_srl 체크
			if(!$this->module_srl) $this->module_srl = Context::get('module_srl');
			if(!$this->module_srl)
			{
				if(!$this->mid) $this->mid = Context::get('mid');
				if(!$this->mid) $this->mid = Context::get('cur_mid');
				if($this->mid)
				{
					$site_module_info = Context::get('site_module_info');
					if($site_module_info) $site_srl = $site_module_info->site_srl;

					// module model 객체 생성
					$oModuleModel = &getModel('module');
					$module_info = $oModuleModel->getModuleInfoByMid($this->mid, $site_srl);
					if($module_info)
					{
						ModuleModel::syncModuleToSite($module_info);
						$this->module_info = $module_info;
						$this->module_srl = $module_info->module_srl;
						Context::set('module_info', $this->module_info);
					}
				}
			}
			// module_srl 입력
			Context::set('module_srl', $this->module_srl);

			// 중요한 모듈 정보 체크
			if(!$this->module_info->mskin) $this->module_info->mskin = 'default';
			// 상담 기능 체크. 현재 게시판의 관리자이면 상담기능을 off시킴, 현재 사용자가 비로그인 사용자라면 글쓰기/댓글쓰기/목록보기/글보기 권한을 제거함
			if($this->module_info->consultation == 'Y' && !$this->grant->manager) {
				$this->consultation = true;
				if(!Context::get('is_logged')) {
					$this->grant->list = $this->grant->write_document = $this->grant->write_comment = $this->grant->view = false;
				}
			} else {
				$this->consultation = false;
			}

			// 스킨 경로를 미리 template_path 라는 변수로 설정함
			$template_path = sprintf('%sm.skins/%s/',$this->module_path, $this->module_info->mskin);
			if(!is_dir($template_path)) return $this->stop('msg_skin_does_not_exist');
			$this->setTemplatePath($template_path);

			// 공통 자바 추가
			Context::addJsFile($this->module_path.'tpl/js/m.module.js');

			//필수 클래스 셋팅
			$oThis = new beluxeItem($this->module_srl);
			Context::set('oThis', $oThis);
			$oEntry = &beluxeEntry::getInstance();
			Context::set('oEntry', $oEntry);
		}

		function dispBeluxeCategory()
		{
			$oThisModel = &getModel(__XEFM_NAME__);
			$cate_list = $oThisModel->getCategoryList($this->module_srl, '.arr');
			Context::set('category_list', $cate_list);
			$this->setTemplateFile('category.html');
		}

		function getBeluxeCommentPage() {
			$document_srl = Context::get('document_srl');
			$oDocumentModel =& getModel('document');
			if(!$document_srl) return new Object(-1, "msg_invalid_request");
			$oDocument = $oDocumentModel->getDocument($document_srl);
			if(!$oDocument->isExists()) return new Object(-1, "msg_invalid_request");
			Context::set('oDocument', $oDocument);
			$oThsModel = &getModel(__XEFM_NAME__);
			$lst_cfg = $oThsModel->getListConfig($this->module_srl);
			Context::set('list_config', $lst_cfg);
			$oTemplate = new TemplateHandler;
			$html = $oTemplate->compile($this->getTemplatePath(), "comment.html");
			$this->add("html", $html);
		}
	}


?>
