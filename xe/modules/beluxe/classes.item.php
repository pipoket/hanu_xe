<?php

	class beluxeItem extends Object
	{
		var $module_srl = 0;

		function beluxeItem($module_srl)
		{
			if(!$module_srl)
			{
				$mid = Context::get('mid');
				if(!$mid) $mid = Context::get('cur_mid');

				$site_module_info = Context::get('site_module_info');
				if($site_module_info) $site_srl = $site_module_info->site_srl;

				$oModuleModel = &getModel('module');
				$module_info = $oModuleModel->getModuleInfoByMid($mid, $site_srl);
				$module_srl = $module_info->module_srl;
			}

			$this->module_srl = $module_srl;
		}

		function getNoticeList()
		{
			$oThisModel = &getModel(__XEFM_NAME__);
			$result = $oThisModel->getNoticeList($this->module_srl);
			if(!$result||!$result->data) return array();
			return $result->data;
		}

		function getBestList()
		{
			$oThisModel = &getModel(__XEFM_NAME__);
			$result = $oThisModel->getBestList($this->module_srl);
			if(!$result||!$result->data) return array();
			return $result->data;
		}

		function getBestCommentList($document_srl)
		{
			$oThisModel = &getModel(__XEFM_NAME__);
			$result = $oThisModel->getBestCommentList($document_srl);
			if(!$result||!$result->data) return array();
			return $result->data;
		}
	}

?>
