/**
 * @author phiDel (phidel@foxb.kr)
 * @update 2011/08/08
 **/

var __XEFM_NAME__ = 'beluxe';

jQuery(function($)
{
	// * 주의 * 아래 코드는 라이센스 관련 코드이므로 절대 변경하시면 안됩니다.
	// Only authorized users can delete the license.
	$('a[href=#beluxe]:eq(0)').each(function(){
		$(this).text('Board DX').attr('href','http://www.foxb.kr/');
		$(this).show().css('font','10px/10px Tahoma, Geneva, sans-serif !important');
	});
});