/**
 * @author phiDel (phidel@foxb.kr)
 * @update 2011/08/08
 **/

var __XEFM_NAME__ = 'beluxe';

jQuery.extend({
	URLEncode:function(c){
		var o='', x=0;c=c.toString(), r=/(^[a-zA-Z0-9_.]*)/;
		while(x<c.length){
			var m=r.exec(c.substr(x));
			if(m!=null && m.length>1 && m[1]!=''){
				o+=m[1];x+=m[1].length;
			}else{
				if(c[x]==' ') o+='+';
				else{
					var d=c.charCodeAt(x);
					var h=d.toString(16);
					o+='%'+(h.length<2?'0':'')+h.toUpperCase();
				}
				x++;
			}
		}
		return o?o:'';
	},

	URLDecode:function(s){
		var o=s, t, r=/(%[^%]{2})/;
		while((m=r.exec(o))!=null && m.length>1 && m[1]!=''){
			b=parseInt(m[1].substr(1),16);
			t=String.fromCharCode(b);
			o=o.replace(m[1],t);
		}
		return o?o:'';
	}
});

String.prototype.trim = function(){
	return this.replace(/^\s+|\s+$/g,"");
};

String.prototype.ucfirst = function(){
	var s=this;
	return s.charAt(0).toUpperCase() + s.slice(1);
};

String.prototype.sprintf = function(){
	if(arguments.length < 1)return this;
	var s=this;
	for(var i=0,c=arguments.length;i<c;i++){
		s=s.replace(/%s/,arguments[i]);
	}
	return s;
};

String.prototype.getEntryQuerys = function() {
	var idx = this.indexOf('?');
	if(idx == -1) return new Array();

	var query_string = this.substr(idx+1, this.length),
		objs = new Array();

	query_string.replace(/([^=]+)=([^&]*)(&|$)/g, function() { objs[arguments[1]] = arguments[2]; });
	if(objs['entry']) {
		var entry = jQuery.URLDecode(objs['entry']);
		entry = entry.split('/');
		for(var i=0,c=entry.length;i<c;i+=2) objs[entry[i]] = entry[i + 1];
	}

	if(arguments.length) {
		var rets = new Array();
		for(var i in arguments){
			rets[arguments[i]] = objs[arguments[i]];
		}
		return rets;
	}
	else return objs;
};

String.prototype.setEntryQuerys = function(args) {
	var url = this,
		patt = /entry=([^&]*)(&|$)/g;

	if(typeof(args) != 'object') args = new Array();

	if(patt.test(url))
	{
		var entry = url.match(patt);
		if(entry){
			entry = jQuery.URLDecode(entry[0]);
			var idx = entry.indexOf('=');
			entry = entry.substr(idx+1, entry.length).split('/');

			for(var i=0,c=entry.length;i<c;i+=2) {
				if(typeof(args[entry[i]]) != 'undefined') continue;
				args[entry[i]] = entry[i + 1];
			}
		}
	}

	url = url.setQuery('entry','');

/* TODO 나중에 하자.
	if(arguments.length) {
		if(typeof(arguments[0]) == 'object'){
			for(var key in arguments[0]) objs[key] = arguments[0][key];
		}else{
			for(var i=0,c=arguments.length;i<c;i+=2) objs[arguments[i]] = arguments[i + 1];
		}
	}
*/

	if(typeof(args) == 'object') {
		for(var key in args) {
			val = args[key];
			url = url.setQuery(key, val);
		}
	}

	return url;
};

jQuery(function($)
{


	// * 주의 * 아래 코드는 라이센스 관련 코드이므로 절대 변경하시면 안됩니다.
	// Only authorized users can delete the license.
	$('a[href=#beluxe]:eq(0)').each(function(){
		$(this).text('Board DX').attr('href','http://www.foxb.kr/');
		$(this).show().css('font','12px/12px Tahoma, Geneva, sans-serif !important');
	});
});
