/**
 * @author phiDel (phidel@foxb.kr)
 * @update 2011/08/08
 **/

var __XEFM_NAME__ = 'beluxe';

String.prototype.ucfirst = function()
{
	var s=this;return s.charAt(0).toUpperCase() + s.slice(1);
};

jQuery(function($)
{
	var tempKey = -1;

	$.fn.fbaSiteMapinit = function(){

		$('form.siteMap', this).delegate('li:not(.placeholder)', 'dropped.st', function() {
			var $this = $(this), $pkey, is_child;

			$pkey = $this.find('>input._parent_key');
			is_child = !!$this.parent('ul').parent('li').length;

			if(is_child) {
				$pkey.val($this.parent('ul').parent('li').find('>input._item_key').val());
			} else {
				$pkey.val('0');
			}
		});

		$('a[href=#addMenu]', this).click(function() {
			var $li = $(addSitemapMenuSample);
			$li.find('>input._item_key').val(tempKey--);
			$li.appendTo($('ul.lined', $(this).closest('form'))).fbaSiteMapinit();
			$('.color-indicator', $li).xe_colorpicker();
			return false;
		});

		$('a[href=#delete]', this).click(function() {
			if(!confirm(xe.lang.confirm_delete)) return false;

			var module_srl = $(this).closest('form').find('>input[name=module_srl]').val(),
				category_srl = $(this).closest('li').find('>input._item_key').val(),
				params = {'module_srl':module_srl,'category_srl':category_srl},
				cateFuncDel = 'proc' + (__XEFM_NAME__).ucfirst() + 'AdminDeleteCategory';

			if(category_srl > 0){
				exec_xml(__XEFM_NAME__, cateFuncDel, params, completeCallModuleAction);
			}else{
				$(this).closest('li').remove();
			}
			return false;
		});

		$('.cbSelect', this).click(function() {
			var $this = $(this),
				$dl = $this.data('dl');

				if($dl == undefined)
				{
					$dl = $('dl', $this);
					$dl.data('type', $this.attr('rel'));
					$this.data('dl', $dl);

					// event 연결은 처음에 한번만 해야지 많이 하면 Overflow
					$('._title', $this).click(function(){$this.click();});
					$(document).mousedown(function(event){
						var target = event.target;
						if ($(target).parents().add(target).index($dl) > -1) return;
						$dl.hide();
					});
					$('dt', $dl).click(function(event) {
						var target = event.target,
							$dt = $(this);
						if(target.tagName == 'INPUT') return;
						if($dl.data('type')=='array')
						{
						}else{
							$('._title', $this).text($dt.text());
							$('._value', $this).val($dt.attr('rel'));
							$('._option', $this).val($dt.attr('rev'));
						}
						$dl.hide();
					});
					$('input[type=checkbox]', $dl).click(function() {
						var ins = new Array();
						$('input:checked', $dl).each(function(i){ins[i] = $(this).val();});
						$('._value', $this).val(ins.join('|@|'));
						$('._title', $this).css('color', ins.length?'red':'');
					});
				}

				$dl.css('top',$this.offset().top + 9 + 'px');
				$dl.css('left',$this.offset().left + 'px');
				$dl.appendTo('body').show();

			return false;
		});
	};

	$.fn.fbaColumninit = function(){
		$('input:checkbox.column_option', this).click(function()
		{
			var par = $(this).closest('div.wrap'),
				option = new Array();
			option[0] = $('input:checkbox#column_display', par).is(':checked')?'Y':'N';
			option[1] = $('input:checkbox#column_sort', par).is(':checked')?'Y':'N';
			option[2] = $('input:checkbox#column_search', par).is(':checked')?'Y':'N';
			$('input:hidden#column_option', par).val(option.join('|@|'));
		});
	};

	$.fn.fbaExtraKeyinit = function(){
		$(this).submit(function(){
			var $eids = $('input._extra_eid', this);
			for(var i=0, c=$eids.length; i<c; i++){
				var sv = $($eids.get(i)).val().trim(),
					patt = /^[^a-z]|[^a-z0-9_]+$/g;
				if(sv==undefined || patt.test(sv)){
					alert('Written rules are different.');
					$($eids.get(i)).focus();
					return false;
				}
			}
			return true;
		});

		$('input:checkbox.extra_option', this).click(function(){
			var par = $(this).closest('div.wrap'),
				option = new Array();
			option[0] = $('input:checkbox#is_required', par).is(':checked')?'Y':'N';
			$('input:hidden#extra_option', par).val(option.join('|@|'));
		});

		$('a[href=#addMenu]', this).click(function() {
			var $tr = $(addExtraKeysSample);
			tempKey--;
			var $ah = $tr.find('.wrap > #name');
				$ah.attr('id', 'name'+tempKey);
			var $ah = $tr.find('.wrap > #desc');
				$ah.attr('id', 'desc'+tempKey);
			var $ah = $tr.find('.wrap > a:eq(0)');
				$ah.attr('href', $ah.attr('href') + '&target=name'+tempKey);
			var $ah = $tr.find('.wrap > a:eq(1)');
				$ah.attr('href', $ah.attr('href') + '&target=desc'+tempKey);
			$tr.appendTo($('#extrakey_list.lined:first', $(this).closest('form'))).fbaExtraKeyinit();
			return false;
		});

		$('a[href=#delete]', this).click(function() {
			if(!confirm(xe.lang.confirm_delete)) return false;

			var module_srl = $(this).closest('form').find('>input[name=module_srl]').val(),
				extra_idx = $(this).closest('.wrap').find('>input._extra_idx').val(),
				params = {'module_srl':module_srl,'extra_idx':extra_idx},
				cateFuncDel = 'proc' + (__XEFM_NAME__).ucfirst() + 'AdminDeleteExtraKey';

			if(extra_idx!=undefined && extra_idx !== 0){
				exec_xml(__XEFM_NAME__, cateFuncDel, params, completeCallModuleAction);
			}else{
				$(this).closest('tr').remove();
			}
			return false;
		});
	};

	$('a.tgMap').click(function() {
		var $this = $(this);
		var curToggleStatus = getCookie('sitemap_toggle_'+$this.attr('href'));
		var toggleStatus = curToggleStatus == 1 ? '0' : 1;

		$($this.attr('href')).slideToggle();
		$this.closest('.siteMap').toggleClass('fold');
		setCookie('sitemap_toggle_'+$this.attr('href'), toggleStatus);

		return false;
	});

	$('a[href=#remakeCache]', this).click(function() {
		var mode = $(this).attr('rel'),
			module_srl = $(this).closest('form').find('>input[name=module_srl]').val(),
			params = {'module_srl':module_srl},
			cateFuncMake = 'proc' + (__XEFM_NAME__).ucfirst() + 'AdminMake' + mode.ucfirst() + 'Cache';
		exec_xml(__XEFM_NAME__, cateFuncMake, params, completeCallModuleAction);
		return false;
	});

	$('a.modalAnchor[href=#deleteForm]').bind('before-open.mw',function(e)
	{
		var $frm = $('#deleteForm'),
			$tr = $(this).closest('tr'),
			aVal = $(this).attr('data-val').split('|@|');

			$('.module_category', $frm).text($('.module_category', $tr).text());
			$('.module_title', $frm).text($('.module_title', $tr).text());
			$('.module_regdate', $frm).text($('.module_regdate', $tr).text());
			$('.module_mid', $frm).text(aVal[0]);
			$('input:hidden[name=module_srl]', $frm).val(aVal[1]);
	});

	$('select.changeLocation').change(function()
	{
		var key = $(this).attr('name'),
			val = $(this).val();
		location.href = decodeURI(current_url).setQuery(key, val);
	});

	$('#siteMapFrame').fbaSiteMapinit();
	$('#columnFrame').fbaColumninit();
	$('#extraKeyFrame').fbaExtraKeyinit();

});
