<?php

	class beluxeView extends beluxe
	{
		var $thVars = array();
		var $oDocModel = null;

	/**************************************************************/
	/*********** @initialization						***********/

		function init()
		{
			// module_srl 체크
			if(!$this->module_srl) $this->module_srl = Context::get('module_srl');
			if(!$this->module_srl)
			{
				if(!$this->mid) $this->mid = Context::get('mid');
				if(!$this->mid) $this->mid = Context::get('cur_mid');
				if($this->mid)
				{
					$site_module_info = Context::get('site_module_info');
					if($site_module_info) $site_srl = $site_module_info->site_srl;

					// module model 객체 생성
					$oModuleModel = &getModel('module');
					$module_info = $oModuleModel->getModuleInfoByMid($this->mid, $site_srl);
					if($module_info)
					{
						ModuleModel::syncModuleToSite($module_info);
						$this->module_info = $module_info;
						$this->module_srl = $module_info->module_srl;
						Context::set('module_info', $this->module_info);
					}
				}
			}
			// module_srl 입력
			Context::set('module_srl', $this->module_srl);

			// 중요한 모듈 정보 체크
			if(!$this->module_info->skin) $this->module_info->skin = 'default';
			// 상담 기능 체크. 현재 게시판의 관리자이면 상담기능을 off시킴, 현재 사용자가 비로그인 사용자라면 글쓰기/댓글쓰기/목록보기/글보기 권한을 제거함
			if($this->module_info->consultation == 'Y' && !$this->grant->manager) {
				$this->consultation = true;
				if(!Context::get('is_logged')) {
					$this->grant->list = $this->grant->write_document = $this->grant->write_comment = $this->grant->view = false;
				}
			} else {
				$this->consultation = false;
			}

			// 스킨 경로를 미리 template_path 라는 변수로 설정함
			$template_path = sprintf('%sskins/%s/',$this->module_path, $this->module_info->skin);
			if(!is_dir($template_path)) return $this->stop('msg_skin_does_not_exist');
			$this->setTemplatePath($template_path);

			// 스킨 언어팩은 따로 읽기
			Context::loadLang($template_path);

			// 공통 자바 추가
			Context::addJsFile($this->module_path.'tpl/js/module.js');

			//필수 클래스 셋팅
			$oThis = new beluxeItem($this->module_srl);
			Context::set('oThis', $oThis);
			$oEntry = &beluxeEntry::getInstance();
			Context::set('oEntry', $oEntry);

			// 팝업이면 팝업 레이아웃으로 설정
			if(Context::get('is_poped')) $this->setLayoutFile('popup_layout');
			if(Context::get('is_iframe')) $this->setLayoutFile('default_layout');
		}

	/**************************************************************/
	/*********** @private function						***********/

		/* @brief set common info */
		function _setBeluxeCommonInfo()
		{
			// 한번만 부르려고 전역 셋팅
			$this->oDocModel = &getModel('document');
			$oThsModel = &getModel(__XEFM_NAME__);

			// 확장 변수 사용시 미리 확장변수의 대상 키들을 가져와서 context set
			$extra_keys = $this->oDocModel->getExtraKeys($this->module_srl);
			Context::set('extra_keys', $extra_keys);

			// 대상 항목을 구함
			$this->thVars['lst_cfg'] = $oThsModel->getListConfig($this->module_srl);
			Context::set('list_config', $this->thVars['lst_cfg']);

			// 카테고리를 사용안하면 제거
			if(!$this->thVars['lst_cfg']['category_srl'] || $this->thVars['lst_cfg']['category_srl']->display != 'Y')
			{
				$this->thVars['cate_list'] = array();
				beluxeEntry::set('category_srl','');
			}else{
				$this->thVars['cate_list'] = $oThsModel->getCategoryList($this->module_srl, '.arr');
				Context::set('category_list', $this->thVars['cate_list']);
			}
		}

		/* @brief get content list */
		function _setBeluxeContentList()
		{
			// 만약 목록 보기 권한이 없을 경우 목록을 보여주지 않음
			if(!$this->grant->list) {
				Context::set('document_list', array());
				Context::set('total_count', 0);
				Context::set('total_page', 1);
				Context::set('page', 1);
				Context::set('page_navigation', new PageHandler(0,0,1,10));
				return;
			}

			$load_extra_vars = true;

			$args = Context::gets('category_srl', 'sort_index', 'order_type', 'page', 'list_count', 'page_count', 'search_target', 'search_keyword');

			// 분류에 navigation 이 있으면 셋팅
			if($args->category_srl)
			{
				$cate_navi = $this->thVars['cate_list'][$args->category_srl];
				if(count($cate_navi) && $cate_navi->navigation)
				{
					$cate_navi = explode(',',$cate_navi->navigation);
					if(!$args->sort_index) $args->sort_index = $cate_navi[0];
					if(!$args->order_type) $args->order_type = $cate_navi[1];
					if(!$args->list_count) $args->list_count = $cate_navi[2];
					if(!$args->page_count) $args->page_count = $cate_navi[3];
				}
			}

			// 지정된 정렬값이 없다면  기본 설정 값을 이용, 확장변수면 eid 값 넣기
			if(!$this->thVars['lst_cfg'][$args->sort_index]) {
				$args->sort_index = $this->module_info->order_target?$this->module_info->order_target:'list_order';
			}elseif($this->thVars['lst_cfg'][$args->sort_index]->idx > 0){
				$args->sort_index = $this->thVars['lst_cfg'][$args->sort_index]->eid;
			}
			if(!in_array($args->order_type, array('asc','desc'))) $args->order_type = $this->module_info->order_type?$this->module_info->order_type:'desc';

			$args->module_srl = $this->module_srl;
			if(!$args->list_count) $args->list_count = ($args->category_srl || $args->search_keyword)?$this->module_info->search_list_count:$this->module_info->list_count;
			if(!$args->page_count) $args->page_count = $this->module_info->page_count;

			// 잘못된 정렬,검색 타겟 재설정
			switch ($args->sort_index)
			{
				case 'no': $args->sort_index = 'list_order'; break;
				case 'custom_status': $args->sort_index = 'is_notice'; break;
			}

			// 상담 기능이 on되어 있으면 현재 로그인 사용자의 글만 나타나도록 옵션 변경
			if($this->consultation) {
				$logged_info = Context::get('logged_info');
				$args->member_srl = $logged_info->member_srl;
			}

			// 특정 문서로 직접 접속할 경우 page값을 직접 구하기 위해 설정
			if($this->thVars['getpage']) {
				$page = $this->oDocModel->getDocumentPage($this->thVars['getpage'], $args);
				Context::set('page', $page);
				$args->page = $page;
			}

			$re_list = $this->oDocModel->getDocumentList($args, true, $load_extra_vars);

			Context::set('document_list', $re_list->data);

			// 페이지 셋팅
			Context::set('total_count', $re_list->total_count);
			Context::set('total_page', $re_list->total_page);
			Context::set('page', $re_list->page);
			Context::set('page_navigation', $re_list->page_navigation);
		}

		/* @brief get content item */
		function _setBeluxeContentView($is_write = false)
		{
			$document_srl = Context::get('document_srl');

			if($document_srl || $is_write)
			{
				$load_extra_vars = true;

				// 해당 콘텐츠 뷰 셋팅
				$re_data = $this->oDocModel->getDocument((int) $document_srl, $this->grant->manager, $load_extra_vars);

				if($re_data->isExists())
				{
					if(!$re_data->isNotice()) {
						// 글 보기 권한을 체크해서 권한이 없으면 빈문서
						$is_empty = !$this->grant->{$is_write?'write_document':'view'} && !$re_data->isGranted();

						// 수정시 비회원 글이고  권한이 없으면 빈문서
						if($is_write && !$is_empty && !$re_data->get('member_srl')) $is_empty = !$re_data->isGranted();

						// 상담기능이 사용되고 사용자의 글도 아니면 빈문서
						if(!$is_empty && $this->consultation) {
							$logged_info = Context::get('logged_info');
							$is_empty = $re_data->get('member_srl') != $logged_info->member_srl;
						}
					}else{
						// 공지는 누구나 볼 수 있게
						$this->grant->view = true;
						if($is_write) $is_empty = !$this->grant->manager;
					}

					// 권한이 없으면 빈문서
					if($is_empty) {
						$re_data = $this->oDocModel->getDocument(0, false, false);
						Context::set('XE_VALIDATOR_MESSAGE_TYPE', 'error');
						Context::set('XE_VALIDATOR_MESSAGE', 'msg_not_permitted');
					} else {
						// 브라우저 타이틀에 글의 제목을 추가
						Context::addBrowserTitle($re_data->getTitleText());// 조회수 증가 (비밀글일 경우 권한 체크)

						// 조회수 증가 (비밀글일 경우 권한 체크)
						if(!$re_data->isSecret() || $re_data->isGranted()) $re_data->updateReadedCount();
						// 비밀글일때 컨텐츠를 보여주지 말자.
						//if($re_data->isSecret() && !$re_data->isGranted()) $re_data->add('content',Context::getLang('thisissecret'));

						// 특정 문서로 직접 접속할 경우 page값을 직접 구하기 위해 설정
						if(!Context::get('page') && !$re_data->isNotice()) $this->thVars['getpage'] = $re_data;

						// 분류값이 있고 다르면 다시 분류 셋팅, 다시 셋팅 이유 selected 값 변경을 위해
						$category_srl = $re_data->get('category_srl');
						if(count($this->thVars['cate_list']) && $category_srl != Context::get('category_srl')){
							beluxeEntry::set('category_srl', $category_srl);
							// 이미 위에서 $oThsModel->getCategoryList 불러서 다시 검사 필요없고 바로 읽음
							@include(__XEDM_CACHE__.sprintf('%s%s.php', $this->module_srl, '.arr'));
							if($menu&&count($menu->list)) {
								$this->thVars['cate_list'] = $menu->list;
								Context::set('category_list', $this->thVars['cate_list']);
							}
						}
					}
				}

				Context::set('oDocument', $re_data);
			}
		}

	/**************************************************************/
	/*********** @public function						***********/

		function dispBoardContent()
		{
			$this->_setBeluxeCommonInfo();
			$this->_setBeluxeContentView();
			$this->_setBeluxeContentList();

			$this->setTemplateFile('index');

			/*
			 * 라이센스 허가된 사용자만 삭제 가능합니다.
			 * Only authorized users can delete the license.
			 */
			Context::addHtmlFooter(
			'<a href="#beluxe" style="display:none">Board DX</a>'
			);
		}

		function dispBoardWrite()
		{
			$this->_setBeluxeCommonInfo();
			$this->_setBeluxeContentView(true);

			$this->setTemplateFile('write');
		}

		function dispBoardDelete()
		{
			$this->dispBoardWrite();
			$this->setTemplateFile('delete');
		}

		function dispBoardWriteComment()
		{
			$this->_setBeluxeCommonInfo();

			// 목록 구현에 필요한 변수들을 가져온다
			$document_srl = Context::get('document_srl');
			$comment_srl = Context::get('comment_srl');
			$parent_srl = Context::get('parent_srl');

			// 해당 댓글를 찾아본다
			$oCommentModel = &getModel('comment');
			$re_data = $oCommentModel->getComment((int) $comment_srl, $this->grant->manager);

			if($re_data->isExists())
			{
				// 글 보기 권한을 체크해서 권한이 없으면 빈문서
				$is_empty = (!$this->grant->write_comment && !$re_data->isGranted());
				// 수정시 비회원 글이고  권한이 없으면 빈문서
				if(!$is_empty && !$re_data->get('member_srl')) $is_empty = !$re_data->isGranted();
			}

			// 문서 번호가 없거나 권한이 없으면 빈문서
			if($is_empty || !$document_srl) {
				$re_data = $oCommentModel->getComment(0, false);
				Context::set('XE_VALIDATOR_MESSAGE_TYPE', 'error');
				Context::set('XE_VALIDATOR_MESSAGE', 'msg_not_permitted');
			}

			$parent_srl = $parent_srl ? $parent_srl : $re_data->get('parent_srl');

			// 필요한 정보들 세팅
			Context::set('document_srl', $document_srl);
			Context::set('oComment', $re_data);
			Context::set('oSourceComment', $oCommentModel->getComment((int) $parent_srl));

			$this->setTemplateFile('write');
		}

		function dispBoardDeleteComment()
		{
			$this->dispBoardWriteComment();
			$this->setTemplateFile('delete');
		}

	/**************************************************************/

	}

?>
