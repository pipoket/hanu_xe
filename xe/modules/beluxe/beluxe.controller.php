<?php

	class beluxeController extends beluxe
	{
	/**************************************************************/
	/*********** @initialization						***********/

		function init()
		{
		}

	/**************************************************************/
	/*********** @private function						***********/

		function _setLocation()
		{
			if(!in_array(Context::getRequestMethod(),array('XMLRPC','JSON')))
			{
				$returnUrl = Context::get('success_return_url');

				if(!$returnUrl && func_num_args())
				{
					$args = func_get_args();
					// act 초기화
					$args[] = 'act'; $args[] = '';
					// Entry 값읽기
					$returnUrl = call_user_func_array(array('beluxeEntry', 'getUrl'), $args);
				}

				if(!$returnUrl) $returnUrl = Context::get('error_return_url');

				if(Context::get('is_iframe'))
				{
					Context::close();
					$script =  sprintf('<script type="text/javascript">parent.location.href = "%s";</script>', $returnUrl);
					exit($script);
				}

				header('location:'.$returnUrl);
			}

			return;
		}

	/**************************************************************/
	/*********** @public function						***********/
	/**************************************************************/

		/* @brief Insert a file url */
		function procBeluxeInsertFileLink() {
			// 필요한 변수 설정
			$sequence_srl = Context::get('sequence_srl');
			$upload_target_srl = Context::get('document_srl');
			$filelink_url = Context::get('filelink_url');
			$module_srl = $this->module_srl;

			if(!preg_match("/^(https?|ftp|file|mms):\/\/[0-9a-z-]+(\.[_0-9a-z-\/\~]+)+(:[0-9]{2,4})*\/[^\/]+[a-zA-Z0-9\:\&\#\@\=\_\~\%\;\?\.\+\-]{3,}/i",$filelink_url))
				return new Object(-1, Context::getLang('msg_invalid_format')."\r\nex: http, ftp, mms, file");

			$filename = basename($filelink_url);
			if(!$filename) return new Object(-1, 'msg_invalid_request');

			if(strpos($filename,'?')>3) $filename = substr($filename,0,strpos($filename,'?'));

			// 업로드 권한이 없거나 정보가 없을시 종료
			if(!$_SESSION['upload_info'][$sequence_srl]->enabled) return new Object(-1, 'msg_not_permitted');

			// upload_target_srl 값이 명시되지 않았을 경우 세션정보에서 추출
			if(!$upload_target_srl) $upload_target_srl = $_SESSION['upload_info'][$sequence_srl]->upload_target_srl;

			// 세션정보에도 정의되지 않았다면 새로 생성
			if(!$upload_target_srl) $_SESSION['upload_info'][$sequence_srl]->upload_target_srl = $upload_target_srl = getNextSequence();

			// 이미지인지 기타 파일인지 체크
			if(preg_match("/\.(png|jpg|jpeg|bmp|gif|ico|swf|flv|mp[1234]|as[fx]|wm[av]|mpg|mpeg|avi|wav|mid|midi|mov|moov|qt|rm|ram|ra|rmm|m4v)$/i", $filename)) {
				// direct 파일에 해킹을 의심할 수 있는 확장자가 포함되어 있으면 바로 삭제함
				// 어차피 링크 파일이라 위험 없음...
				//if(preg_match("/\.(php|phtm|html|htm|cgi|pl|exe|jsp|asp|inc)/i",$filename)) return;

				$filename = str_replace(array('<','>'),array('%3C','%3E'), $filename);
				$direct_download = 'Y';
			} else {
				$direct_download = 'N';
			}

			// 사용자 정보를 구함
			$oMemberModel = &getModel('member');
			$member_srl = $oMemberModel->getLoggedMemberSrl();

			// 파일 정보를 정리
			$args->file_srl = getNextSequence();
			$args->upload_target_srl = $upload_target_srl;
			$args->module_srl = $module_srl;
			$args->direct_download = $direct_download;
			$args->source_filename = $filename;
			$args->uploaded_filename = $filelink_url;
			$args->download_count = 0;
			$args->file_size = 0;
			$args->comment = 'link';
			$args->member_srl = $member_srl;
			$args->sid = md5(rand(rand(1111111,4444444),rand(4444445,9999999)));

			$output = executeQuery('file.insertFile', $args);
			if(!$output->toBool()) return $output;

			$this->add('sequence_srl', $sequence_srl);
			$this->setMessage('success_registed');
		}

		function procBoardVerificationPassword()
		{
			// 비밀번호와 문서 번호를 받음
			$password = Context::get('password');

			$document_srl = Context::get('document_srl');
			$comment_srl = Context::get('comment_srl');

			$oMemberModel = &getModel('member');

			// comment_srl이 있을 경우 댓글이 대상
			if($comment_srl) {
				// 문서번호에 해당하는 글이 있는지 확인
				$oCommentModel = &getModel('comment');
				$oComment = $oCommentModel->getComment($comment_srl, false, array('comment_srl','password'));
				if(!$oComment->isExists()) return new Object(-1, 'msg_not_founded');

				// 문서의 비밀번호와 입력한 비밀번호의 비교
				if(!$oMemberModel->isValidPassword($oComment->get('password'),$password)) return new Object(-1, 'msg_invalid_password');

				$oComment->setGrant();
			} else {
				// 문서번호에 해당하는 글이 있는지 확인
				$oDocumentModel = &getModel('document');
				$oDocument = $oDocumentModel->getDocument($document_srl, false, false, array('document_srl','password'));
				if(!$oDocument->isExists()) return new Object(-1, 'msg_not_founded');

				// 문서의 비밀번호와 입력한 비밀번호의 비교
				if(!$oMemberModel->isValidPassword($oDocument->get('password'),$password)) return new Object(-1, 'msg_invalid_password');

				$oDocument->setGrant();
			}

			$this->_setLocation();
		}

		function procBoardInsertDocument()
		{
			if(!$this->grant->write_document) return new Object(-1, 'msg_not_permitted');
// TODO 카테고리 grant 체크해라, 근데 코어에서 해야될거 같거든?
			$args = Context::getRequestVars();

			$module_srl = (int) $args->module_srl;
			$item_srl = (int) $args->document_srl;
			if(!$module_srl || $this->module_srl != $module_srl) return new Object(-1,'msg_invalid_request');

			$logged_info = Context::get('logged_info');

			// 회원이라면 닉,암호 제거
			if(Context::get('is_logged'))
			{
				unset($args->nick_name);
				unset($args->password);
			}

			// 관리자가 아니라면 게시글 색상/굵기 제거
			if(!$this->grant->manager)
			{
				unset($args->title_color);
				unset($args->title_bold);
			}

			$args->commentStatus = $args->comment_status;
			unset($args->comment_status);

			// 사용 상태에 없는 값이면 임시로 설정
			$use_status = explode(',', $this->module_info->use_status);
			if(!in_array($args->status, $use_status)) $args->status = count($use_status)?'TEMP':'PUBLIC';

			if($args->is_notice!='Y'||!$this->grant->manager) $args->is_notice = 'N';

			// 관리자이고 공지가 아니면 is_notice 필드에 상태입력
			// TODO is_notice 필드 사용하는이유는 상태를 넣을 필드가 없어서...
			if($this->module_info->custom_status && $this->grant->manager && $args->is_notice!='Y')
			{
				$args->is_notice = (int) $args->custom_status;
				$args->is_notice = ($args->is_notice < 1 && $args->is_notice > 9)?'N':((string) $args->is_notice);
			}

			settype($args->title, "string");
			if($args->title == '') $args->title = cut_str(strip_tags($args->content),20,'...');
			if($args->title == '') $args->title = 'Untitled';

			if($args->tags) $args->tags = preg_replace('/\s+/', ',', strip_tags($args->tags));

			// 익명 설정일 경우 여러가지 요소를 미리 제거 (알림용 정보들 제거)
			if($this->module_info->use_anonymous == 'Y') {
				$args->notify_message = 'N';
				$this->module_info->admin_mail = '';
				$args->member_srl = -1*$logged_info->member_srl;
				$args->email_address = $args->homepage = $args->user_id = '';
				$args->user_name = $args->nick_name = 'anonymous';
				$bAnonymous = true;
			}
			else
			{
				$bAnonymous = false;
			}

			// document module의 객체 생성
			$oDocumentModel = &getModel('document');
			$oDocumentController = &getController('document');

			// 이미 존재하는 글인지 체크
			$oDocument = $oDocumentModel->getDocument($item_srl, false, false); // TODO 캐시 때문에 안됨, 이슈등록, array('document_srl', 'category_srl')
			if($oDocument->isExists() && $oDocument->get('module_srl') != $module_srl) return new Object(-1,'msg_invalid_request');

			// TODO ruleset 확장 변수 체크 코어에서 고칠때까지 임시 조치
			foreach($args as $key=>$val){
				if(strpos($key,'extra_vars')===false) continue;
				if(is_array($val)) $args->{$key} = implode('|@|',$val);
			}

			// 이미 존재하는 경우 수정
			if($oDocument->isExists()) {

				if(!$oDocument->isGranted()) return new Object(-1,'msg_not_permitted');
				$output = $oDocumentController->updateDocument($oDocument, $args);

				$is_updateCategoryCount = $oDocument->get('category_srl') != $args->category_srl;
				$msg_code = 'success_updated';
				$page = Context::get('page');

			// 그렇지 않으면 신규 등록
			} else {
				// 신규에 srl 이 있으면 첨부 파일이 들어있는 경우
				$output = $oDocumentController->insertDocument($args, $bAnonymous);
				$item_srl = $output->get('document_srl');

				// 문제가 없고 모듈 설정에 관리자 메일이 등록되어 있으면 메일 발송
				if($output->toBool() && $this->module_info->admin_mail) {
					$oMail = new Mail();
					$oMail->setTitle($args->title);
					$oMail->setContent( sprintf("From : <a href=\"%s\">%s</a><br/>\r\n%s", getFullUrl('','document_srl',$item_srl), getFullUrl('','document_srl',$item_srl), $args->content));
					$oMail->setSender($args->user_name, $args->email_address);

					$target_mail = explode(',',$this->module_info->admin_mail);
					for($i=0;$i<count($target_mail);$i++) {
						$email_address = trim($target_mail[$i]);
						if(!$email_address) continue;
						$oMail->setReceiptor($email_address, $email_address);
						$oMail->send();
					}
				}

				$is_updateCategoryCount = true;
				$msg_code = 'success_registed';
			}

			// 오류 발생시 멈춤
			if(!$output->toBool()) return $output;

			// 캐쉬 갱신
			if($is_updateCategoryCount)
			{
				require_once(__XEFM_PATH__.'classes.cache.php');
				beluxeCache::categoryList($module_srl);
			}

			$document_srl = $output->get('document_srl');

			$this->setMessage($msg_code);
			$this->_setLocation('document_srl', $document_srl);
		}

		function procBoardInsertComment()
		{
			// 권한 체크
			if(!$this->grant->write_comment) return new Object(-1, 'msg_not_permitted');

			$args = Context::getRequestVars();

			$module_srl = (int) $args->module_srl;
			$document_srl = (int) $args->document_srl;
			$item_srl = (int) $args->comment_srl;
			if(!$document_srl || !$module_srl || $this->module_srl != $module_srl) return new Object(-1,'msg_invalid_request');

			$logged_info = Context::get('logged_info');

			// 회원이라면 닉,암호 제거
			if(Context::get('is_logged'))
			{
				unset($args->nick_name);
				unset($args->password);
			}

			// 사용 상태에 없는 값이면 비밀로 설정
			$use_status = explode(',', $this->module_info->use_c_status);
			if(count($use_status) && !in_array($args->status, $use_status)) $args->status = 'SECRET';
			$args->is_secret = $args->status == 'SECRET'?'Y':'N';
			unset($args->status);

			// 원글이 존재하는지 체크
			$oDocumentModel = &getModel('document');
			$oDocument = $oDocumentModel->getDocument($document_srl, false, false); // TODO 캐시 때문에 안됨, 이슈등록 , array('document_srl','category_srl')
			if(!$oDocument->isExists() || $oDocument->get('module_srl') != $module_srl) return new Object(-1,'msg_invalid_request');

			// For anonymous use, remove writer's information and notifying information
			if($this->module_info->use_anonymous == 'Y') {
				$args->notify_message = 'N';
				$this->module_info->admin_mail = '';

				$args->member_srl = -1*$logged_info->member_srl;
				$args->email_address = $args->homepage = $args->user_id = '';
				$args->user_name = $args->nick_name = 'anonymous';
				$bAnonymous = true;
			}
			else
			{
				$bAnonymous = false;
			}

			// comment 모듈의  객체 생성
			$oCommentModel = &getModel('comment');
			$oCommentController = &getController('comment');

			// 체크
			$oComment = $oCommentModel->getComment($item_srl, false); // TODO 캐시 때문에 안됨, 이슈등록 , array('comment_srl','parent_srl')
			if($oComment->isExists() && $oComment->get('module_srl') != $module_srl) return new Object(-1, 'msg_invalid_request');

			// comment 있으면 수정으로
			if($oComment->isExists()) {
				// 다시 권한체크
				if(!$oComment->isGranted()) return new Object(-1,'msg_not_permitted');

				$args->parent_srl = $oComment->parent_srl;
				$output = $oCommentController->updateComment($args, $this->grant->manager);

				$msg_code = 'success_updated';

			// 없을 경우 신규 입력
			} else {
				// parent_srl이 있으면 체크
				if((int) $args->parent_srl) {
					$parent_comment = $oCommentModel->getComment($args->parent_srl, false); // TODO 캐시 때문에 안됨, 이슈등록 , array('comment_srl')
					if(!$parent_comment->comment_srl) return new Object(-1, 'msg_invalid_request');
				}else{
					unset($args->parent_srl);
				}

				$output = $oCommentController->insertComment($args, $bAnonymous);

				// 문제가 없고 모듈 설정에 관리자 메일이 등록되어 있으면 메일 발송
				if($output->toBool() && $this->module_info->admin_mail) {
					$oMail = new Mail();
					$oMail->setTitle($oDocument->getTitleText());
					$oMail->setContent( sprintf("From : <a href=\"%s#comment_%d\">%s#comment_%d</a><br/>\r\n%s", getFullUrl('','document_srl',$document_srl),$item_srl, getFullUrl('','document_srl',$document_srl), $item_srl, $args->content));
					$oMail->setSender($args->user_name, $args->email_address);

					$target_mail = explode(',',$this->module_info->admin_mail);
					for($i=0;$i<count($target_mail);$i++) {
						$email_address = trim($target_mail[$i]);
						if(!$email_address) continue;
						$oMail->setReceiptor($email_address, $email_address);
						$oMail->send();
					}
				}

				$msg_code = 'success_registed';
			}

			// 오류 발생시 멈춤
			if(!$output->toBool()) return $output;

			$comment_srl = $output->get('comment_srl');

			$this->setMessage($msg_code);
			$this->_setLocation('document_srl', $document_srl, 'comment_srl', $comment_srl);
		}

		function procBoardDeleteDocument()
		{
			$args = Context::getRequestVars();

			$module_srl = (int) $args->module_srl;
			$item_srl = (int) $args->document_srl;

			// 문서 번호가 없다면 오류 발생
			if(!$item_srl) return new Object(-1,'msg_invalid_document');
			if(!$module_srl || $this->module_srl != $module_srl) return new Object(-1,'msg_invalid_request');

			// 삭제 시도
			$oDocumentController = &getController('document');
			$output = $oDocumentController->deleteDocument($item_srl, $this->grant->manager);
			if(!$output->toBool()) return $output;

			require_once(__XEFM_PATH__.'classes.cache.php');
			beluxeCache::categoryList($module_srl);

			$this->setMessage('success_deleted');
			$this->_setLocation('document_srl', '');
		}

		function procBoardDeleteComment()
		{
			$args = Context::getRequestVars();

			$module_srl = (int) $args->module_srl;
			$item_srl = (int) $args->comment_srl;

			// 문서 번호가 없다면 오류 발생
			if(!$item_srl) return new Object(-1,'msg_invalid_document');
			if(!$module_srl || $this->module_srl != $module_srl) return new Object(-1,'msg_invalid_request');

			// 삭제 시도
			$oCommentController = &getController('comment');
			$output = $oCommentController->deleteComment($item_srl, $this->grant->manager);
			if(!$output->toBool()) return $output;

			$document_srl = $output->get('document_srl');

			$this->setMessage('success_deleted');
			$this->_setLocation('document_srl', $document_srl, 'comment_srl', '');
		}

		function procBeluxeDeleteTrackback() {
			$trackback_srl = Context::gets('trackback_srl');
			if(!$trackback_srl) return new Object(-1,'msg_invalid_request');

			// trackback module의 controller 객체 생성
			$oTrackbackController = &getController('trackback');
			$output = $oTrackbackController->deleteTrackback($trackback_srl, $this->grant->manager);
			if(!$output->toBool()) return $output;

			$this->setMessage('success_deleted');
			$this->_setLocation('document_srl', $output->get('document_srl'));
		}

		function procBeluxeChangeCustomStatus()
		{
			$target_srl = Context::get('target_srl');
			//if(!$target_srl)
			return new Object(-1,'msg_invalid_request');

			$target_srl = explode('|@|',$target_srl);
			$oDocumentController = &getController('document');

			foreach($target_srl as $val){
			}

			$this->setMessage('success_updated');
		}

	}

?>
