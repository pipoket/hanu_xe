<?php
	/**
	 * @class Cache
	 * @author phiDel (phidel@foxb.kr)
	 * @brief Make Cache classes
	 * @version 2011/09/25
	 **/

	class beluxeCache
	{
		/* @brief Create a cache file of category */
		function categoryList($module_srl, $is_xml = false)
		{
			// Return if there is no information you need for creating a cache file
			if(!$module_srl) return;

			/* @brief change sorted nodes in an array to the php code and then return
			 * when using menu on tpl, you can directly xml data. howver you may need javascrips additionally.
			 * therefore, you can configure the menu info directly from php cache file, not through DB.
			 * You may include the cache in the ModuleHandler::displayContent() */
			function _getPhpCacheCode($source_node, $tree, $depth, $site_srl, &$php_header_buff)
			{
				if(!$source_node) return $output;

				$depth++;
				$output = array("ori_buff"=>"", "buff"=>"", "item_count_list"=>0, "category_srl_list"=>array());

				$oModuleAdminModel = &getAdminModel('module');

				// Set to an arraty for looping and then generate php script codes to be included
				foreach($source_node as $category_srl => $node)
				{
					// Get data from child nodes first if exist.
					if($category_srl&&$tree[$category_srl]) $child_output = _getPhpCacheCode($tree[$category_srl], $tree, $depth, $site_srl, $php_header_buff);
					else $child_output = array("buff"=>"", "item_count_list"=>0, "category_srl_list"=>array());

					// If node->group_srls value exists
					if($node->group_srls) $group_check_code = sprintf('($is_admin==true||(is_array($group_srls)&&count(array_intersect($group_srls, array(%s)))))',$node->group_srls);
					else $group_check_code = 'true';
					$expand = $node->expand;

					$child_buff = $child_output['buff'];
					$child_ori_buff = $child_output['ori_buff'];

					$total_document_count = $child_output['item_count_list'];

					$childs = implode('","',$child_output['category_srl_list']);
					$child_output['category_srl_list'][] = $node->category_srl;
					$selected = '"'.implode('","',$child_output['category_srl_list']).'"';

					// Set values into category_srl_list arrary if url of the current node is not empty
					$output['category_srl_list'] = array_merge($output['category_srl_list'], $child_output['category_srl_list']);
					$output['item_count_list'] += $node->document_count;

					$title = $node->title;
					// unserialize type and description
					if(strpos($node->description,'|@|')===false)
					{
						$type = '';
						$navigation = '';
						$description = $node->description;
					}else{
						$description = explode('|@|',$node->description);
						$type = $description[0];
						$navigation = $description[1];
						$description = $description[2];
					}

					$langs = $oModuleAdminModel->getLangCode($site_srl, $title);
					if(count($langs)) foreach($langs as $key => $val) $php_header_buff .= sprintf('$_titles[%d]["%s"] = "%s"; ', $category_srl, $key, str_replace('"','\\"',htmlspecialchars($val)));
					$langx = $oModuleAdminModel->getLangCode($site_srl, $description);
					if(count($langx)) foreach($langx as $key => $val) $php_header_buff .= sprintf('$_descriptions[%d]["%s"] = "%s"; ', $category_srl, $key, str_replace('"','\\"',htmlspecialchars($val)));

					// Create attributes(Use the category_srl_list to check whether to belong to the menu's node. It seems to be tricky but fast fast and powerful;)
					$attribute = sprintf(
						'"mid" => "%s","module_srl" => "%d","node_srl"=>"%s","category_srl"=>"%s","root_srl"=>"%s","parent_srl"=>"%s","text"=>$_titles[%d][$lang_type],"selected"=>(in_array(Context::get("category_srl"),array(%s))?1:0),"expand"=>"%s","color"=>"%s","description"=>$_descriptions[%d][$lang_type],"list"=>array(%s),"item_count"=>"%d","group_srls"=>array(%s),"type"=>"%s","navigation"=>"%s","grant"=>%s?1:0',
						$node->mid,
						$node->module_srl,
						$node->category_srl,
						$node->category_srl,
						$node->root_srl,
						$node->parent_srl,
						$node->category_srl,
						$selected,
						$expand,
						$node->color,
						$node->category_srl,
						$child_ori_buff,
						$node->document_count,
						$node->group_srls,
						$type,
						$navigation,
						$group_check_code
					);
					// Generate buff data
					$output['ori_buff'] .=  sprintf('%s=>array(%s),', $node->category_srl, $attribute);

					// Create attributes(Use the category_srl_list to check whether to belong to the menu's node. It seems to be tricky but fast fast and powerful;)
					$attribute = sprintf(
						'"depth" => "%s","mid" => "%s","module_srl" => "%d","category_srl"=>"%s","parent_srl"=>"%s","title"=>$_titles[%d][$lang_type],"selected"=>(in_array(Context::get("category_srl"),array(%s))?1:0),"expand"=>%s?1:0,"color"=>"%s","description"=>$_descriptions[%d][$lang_type],"document_count"=>"%d","total_document_count"=>"%d","childs"=>array(%s),"type"=>"%s","navigation"=>"%s","grant"=>%s?1:0',
						$depth,
						$node->mid,
						$node->module_srl,
						$node->category_srl,
						$node->parent_srl,
						$node->category_srl,
						$selected,
						$expand=='Y'?'true':'false',
						$node->color,
						$node->category_srl,
						$node->document_count,
						$total_document_count + $node->document_count,
						$childs?'"'.$childs.'"':'',
						$type,
						$navigation,
						$group_check_code
					);

					$output['buff'] .=  sprintf('%s=>(object)array(%s),', $node->category_srl, $attribute);
					if($child_buff) $output['buff'] .=  sprintf('%s', $child_buff);
				}

				return $output;
			}

			/* @brief Create the xml data recursively referring to parent_srl
			 * In the menu xml file, node tag is nested and xml doc enables the admin page to have a menu\n
			 * (tree menu is implemented by reading xml file from the tree_menu.js) */
			function _getXmlTree($source_node, $tree, $site_srl, &$xml_header_buff)
			{
				if(!$source_node) return;

				$oModuleAdminModel = &getAdminModel('module');

				foreach($source_node as $category_srl => $node)
				{
					$child_buff = "";
					// Get data of the child nodes
					if($category_srl && $tree[$category_srl]) $child_buff = _getXmlTree($tree[$category_srl], $tree, $site_srl, $xml_header_buff);
					// List variables
					$expand = $node->expand;
					$group_srls = $node->group_srls;
					$mid = $node->mid;
					$module_srl = $node->module_srl;
					$parent_srl = $node->parent_srl;
					$color = $node->color;
					// unserialize type and description
					if(strpos($node->description,'|@|')===false)
					{
						$type = '';
						$navigation = '';
						$description = $node->description;
					}else{
						$description = explode('|@|',$node->description);
						$type = $description[0];
						$navigation = $description[1];
						$description = $description[2];
					}
					// If node->group_srls value exists
					if($group_srls) $group_check_code = sprintf('($is_admin==true||(is_array($group_srls)&&count(array_intersect($group_srls, array(%s)))))',$group_srls);
					else $group_check_code = 'true';

					$title = $node->title;
					$langs = $oModuleAdminModel->getLangCode($site_srl, $title);
					if(count($langs)) foreach($langs as $key => $val) $xml_header_buff .= sprintf('$_titles[%d]["%s"] = "%s"; ', $category_srl, $key, str_replace('"','\\"',htmlspecialchars($val)));

					$langx = $oModuleAdminModel->getLangCode($site_srl, $description);
					if(count($langx)) foreach($langx as $key => $val) $xml_header_buff .= sprintf('$_descriptions[%d]["%s"] = "%s"; ', $category_srl, $key, str_replace('"','\\"',htmlspecialchars($val)));

					$attribute = sprintf(
							'mid="%s" module_srl="%d" node_srl="%d" parent_srl="%d" category_srl="%d" text="<?php echo (%s?($_titles[%d][$lang_type]):"")?>" url="%s" expand="%s" color="%s" type="%s" navigation="%s" description="<?php echo (%s?($_descriptions[%d][$lang_type]):"")?>" document_count="%d"',
							$mid,
							$module_srl,
							$category_srl,
							$parent_srl,
							$category_srl,
							$group_check_code,
							$category_srl,
							getUrl('','mid',$node->mid,'category_srl',$category_srl),
							$expand,
							$color,
							$type,
							$navigation,
							$group_check_code,
							$category_srl,
							$node->document_count
							);

					if($child_buff) $buff .= sprintf('<node %s>%s</node>', $attribute, $child_buff);
					else $buff .=  sprintf('<node %s />', $attribute);
				}
				return $buff;
			}

			// __XEDM_CACHE__ 도큐먼트 모듈과 호환을 위해 같이 사용.
			if(!is_dir(__XEDM_CACHE__)) FileHandler::makeDir(__XEDM_CACHE__);

			// Cache file's name
			$ori_file = __XEDM_CACHE__.sprintf('%s.php', $module_srl);
			$php_file = __XEDM_CACHE__.sprintf('%s.arr.php', $module_srl);
			if($is_xml) $xml_file = __XEDM_CACHE__.sprintf('%s.xml.php', $module_srl);

			// Get a category list
			$args->module_srl = $module_srl;
			$args->sort_index = 'list_order';
			$output = executeQueryArray('document.getCategoryList', $args);
			if(!$output->data)
			{
				FileHandler::removeFile($ori_file);
				FileHandler::removeFile($php_file);
				if($is_xml) FileHandler::removeFile($xml_file);
				return;
			}

			$category_list = is_array($output->data)?$output->data:array($output->data);
			$category_count = count($category_list);

			for($i=0;$i<$category_count;$i++)
			{
				$category_srl = $category_list[$i]->category_srl;
				if(!preg_match('/^[0-9,]+$/', $category_list[$i]->group_srls)) $category_list[$i]->group_srls = '';
				$list[$category_srl] = $category_list[$i];
			}

			// Create the xml file without node data if no data is obtained
			if(!$list)
			{
				FileHandler::writeFile($ori_file, '<?php if(!defined("__XE__"))exit; ?>');
				FileHandler::writeFile($php_file, '<?php if(!defined("__XE__"))exit; ?>');
				if($is_xml) FileHandler::writeFile($xml_file, '<root />');
				return;
			}

			// Get module information (to obtain mid)
			$oModuleModel = &getModel('module');
			$module_info = $oModuleModel->getModuleInfoByModuleSrl($module_srl, array('mid', 'site_srl'));
			$mid = $module_info->mid;

			// Create a tree for loop
			foreach($list as $category_srl => $node)
			{
				$node->mid = $mid;
				$parent_srl = (int)$node->parent_srl;
				$tree[$parent_srl][$category_srl] = $node;
			}

			// A common header to set permissions and groups of the cache file
			$header_script =
				'$lang_type = Context::getLangType(); '.
				'$is_logged = Context::get(\'is_logged\'); '.
				'$logged_info = Context::get(\'logged_info\'); '.
				'if($is_logged) {'.
					'if($logged_info->is_admin=="Y") $is_admin = true; '.
					'else $is_admin = false; '.
					'$group_srls = array_keys($logged_info->group_list); '.
				'} else { '.
					'$is_admin = false; '.
					'$group_srsl = array(); '.
				'} ';

			// Create the xml cache file (a separate session is needed for xml cache)
			if($is_xml){
				$xml_header_buff = '';
				$xml_body_buff = _getXmlTree($tree[0], $tree, $module_info->site_srl, $xml_header_buff);
				$xml_buff = sprintf(
						'<?php define(\'__ZBXE__\', true); '.
						'define(\'__XE__\', true); '.
						'require_once(\''.FileHandler::getRealPath('./config/config.inc.php').'\'); '.
						'$oContext = &Context::getInstance(); '.
						'$oContext->init(); '.
						'header("Content-Type: text/xml; charset=UTF-8"); '.
						'header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); '.
						'header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); '.
						'header("Cache-Control: no-store, no-cache, must-revalidate"); '.
						'header("Cache-Control: post-check=0, pre-check=0", false); '.
						'header("Pragma: no-cache"); '.
						'%s'.
						'%s '.
						'$oContext->close();'.
						'?>'.
						'<root>%s</root>',
						$header_script,
						$xml_header_buff,
						$xml_body_buff
				);
			}

			// Create php cache file
			$php_output = _getPhpCacheCode($tree[0], $tree, -1, $module_info->site_srl, $php_header_buff);
			$php_buff = sprintf(
					'<?php '.
					'if(!defined("__XE__"))exit; '.
					'%s; '.
					'%s; '.
					'$menu->list = array(%s); '.
					'?>',
					$header_script,
					$php_header_buff,
					$php_output['buff']
			);
			$ori_buff = sprintf(
					'<?php '.
					'if(!defined("__XE__"))exit; '.
					'%s; '.
					'%s; '.
					'$menu->list = array(%s); '.
					'?>',
					$header_script,
					$php_header_buff,
					$php_output['ori_buff']
			);

			FileHandler::writeFile($ori_file, $ori_buff);
			FileHandler::writeFile($php_file, $php_buff);
			if($is_xml) FileHandler::writeFile($xml_file, $xml_buff);
		}

		/* @brief Create a cache of column config */
		function columnConfigList($module_srl)
		{
			// 저장된 목록 설정값을 구하고 없으면 기본 값으로 설정
			$oModuleModel = &getModel('module');
			$list_config = $oModuleModel->getModulePartConfig('beluxe', $module_srl);

			$virtual_vars = array(
				'no'=>array('1','','Y','N','N'),
				'category_srl'=>array('2','','N','N','N'),
				'title'=>array('3','','Y','N','Y'),
				'nick_name'=>array('4','','Y','N','Y'),
				'user_name'=>array('5'),
				'user_id'=>array('6'),
				'last_updater'=>array('7'),
				'email_address'=>array('8'),
				'homepage'=>array('9'),
				'ipaddress'=>array('10'),
				'voted_count'=>array('11'),
				'blamed_count'=>array('12'),
				'readed_count'=>array('13','','Y','Y','N'),
				'regdate'=>array('14','','Y','Y','Y'),
				'last_update'=>array('15'),
				'custom_status'=>array('16'),
				'content'=>array('17','','N','N','Y'),
				'comment'=>array('18','','N','N','Y'),
				'thumbnail'=>array('19')
			);

			$args->module_srl = $module_srl;
			$args->sort_index = 'var_idx';
			$args->order = 'asc';
			$output = executeQueryArray('document.getDocumentExtraKeys', $args);
			if($output->toBool() && count($output->data)) $virtual_vars = array_merge($virtual_vars, $output->data);

			foreach($virtual_vars as $key=>$val) {
				if(gettype($val) == 'object'){
					$kname = 'extra_vars'.$val->idx;
					if($item = $list_config[$kname]){
						$aSort[$kname] = (9000-$item[0])*-1;
						$val->color = $item[1];
						$val->display = $item[2];
						$val->sort = $item[3];
						$val->search = $item[4];
					}else{
						$aSort[$kname] = 9000 + $key;
					}
					$extra_vars[$kname] = array(
						$module_srl, $val->idx, $val->name, $val->type, $val->default, $val->desc,
						$val->is_required, $val->search, $val->value, $val->eid, $val->color, $val->display, $val->sort
					);
				}else{
					if($item = $list_config[$key]){
						$aSort[$key] = (9000-$item[0])*-1;
						$val = $item;
					}else{
						$aSort[$key] = count($aSort)+1;
					}
					$extra_vars[$key] = array(
						$module_srl, -1, 'Context::getLang(\''.$key.'\')', 'text', 'N', '\'\'',
						'N', (string)$val[4], '', $key, (string)$val[1], (string)$val[2], (string)$val[3]
					);
				}
			}

			array_multisort($aSort, $extra_vars);

			// Get module information (to obtain mid)
			$oModuleModel = &getModel('module');
			$module_info = $oModuleModel->getModuleInfoByModuleSrl($module_srl, array('mid', 'site_srl'));
			$mid = $module_info->mid;

			$oModuleAdminModel = &getAdminModel('module');
			$site_srl = $module_info->site_srl;

			$php_buff = '$lang_type = Context::getLangType(); $list_config = array(); ';

			foreach($extra_vars as $key=>$val){

				if($val[1] > 0)
				{
					$langs = $oModuleAdminModel->getLangCode($site_srl, $val[2]);
					if(count($langs)) foreach($langs as $lakey => $lang) $php_buff .= sprintf('$_titles[%d]["%s"] = "%s"; ', $val[1], $lakey, str_replace('"','\\"',htmlspecialchars($lang)));
					$langs = $oModuleAdminModel->getLangCode($site_srl, $val[5]);
					if(count($langs)) foreach($langs as $lakey => $lang) $php_buff .= sprintf('$_descriptions[%d]["%s"] = "%s"; ', $val[1], $lakey, str_replace('"','\\"',htmlspecialchars($lang)));
					$val[2] = '$_titles['.$val[1].'][$lang_type]';
					$val[5] = '$_descriptions['.$val[1].'][$lang_type]';
				}

				$php_buff .=
					'$list_config[\''.$key.'\']='.
					'new ExtraItem('.
						$val[0].','.
						$val[1].','.
						$val[2].','.
						'\''.$val[3].'\','.
						'\''.$val[4].'\','.
						$val[5].','.
						'\''.$val[6].'\','.
						'\''.$val[7].'\','.
						'"'.str_replace('"','\\"',htmlspecialchars($val[8])).'",'.
						'\''.$val[9].'\''.
					');'.
					'$list_config[\''.$key.'\']->color=\''.$val[10].'\';'.
					'$list_config[\''.$key.'\']->display=\''.$val[11].'\';'.
					'$list_config[\''.$key.'\']->sort=\''.$val[12].'\'; '
				;
			}

			if(!is_dir(__XEFM_CACHE__)) FileHandler::makeDir(__XEFM_CACHE__);
			$php_file = __XEFM_CACHE__.sprintf('%s.col.php', $module_srl);
			FileHandler::writeFile($php_file, sprintf('<?php if(!defined("__XE__"))exit; %s ?>', $php_buff));
		}
	}
?>
