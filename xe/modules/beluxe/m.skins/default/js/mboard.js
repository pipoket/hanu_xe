function loadPage(document_srl, page) {
	var params = {};
	params["cpage"] = page;
	params["document_srl"] = document_srl;
	params["mid"] = current_mid;

	exec_xml('beluxe', 'getBeluxeCommentPage', params,
	function(ret){
		jQuery("#cl").remove();
		jQuery("#clpn").remove();
		jQuery("#clb").parent().after(ret['html']);
	}
	, ['html','error','message']);
}

jQuery(function($)
		{

// TODO ruleset 확장 변수 체크 코어에서 고칠때까지 임시 조치
$('form .exvar:eq(0)').each(function(){
	var $form = $(this).closest('form');
	$form.submit(function(){
		var $li = $('form li.exvar');

		if($li.length) {
			for(var i=0, c=$li.length;i<c;i++){
				var $put = $('input[name],select[name],textarea[name]',$li.get(i)),
					req = $('.required', $li.get(i)).text();

				if($put.length){
					var name = $put.attr('name'),
						oooo = $put.get(0);

					if(!$put.is('[type=radio]') && $put.length > 1 && name.indexOf('[') == -1) $put.attr('name', name+'[]');

					if($put.is('[type=checkbox]'))$put = $('input:checked',$li.get(i));
					if($put.is('[type=radio]')) $put = $('input:checked',$li.get(i));

					var vals = new Array();
					for(var k=0, u=$put.length;k<u;k++){
						var vv = $($put.get(k)).val();
						if(typeof(vv)=='string'&&vv) vals[k] = vv;
					}

					var val = vals.join('|@|');
					if(req && !val){
						alert(name + ': required');
						oooo.focus();
						return false;
					}
				}
			}
		}
	});
});
		});
