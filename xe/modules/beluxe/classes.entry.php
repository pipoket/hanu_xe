<?php

	class beluxeEntry extends Object
	{
		var $ent = '';
		var $rqv = array();
		var $not = array('act','vid','mid','key');

		function &getInstance()
		{
			static $theInstance = null;
			if(!$theInstance) $theInstance = new beluxeEntry();
			return $theInstance;
		}

		function beluxeEntry()
		{
			$this->rqv = $_GET;
			$this->rqv['vid'] = Context::get('vid');
			$this->rqv['mid'] = Context::get('mid');

			$ent = urldecode($this->rqv['entry']);

			// 필요없는거 삭제
			unset($this->rqv['entry']);
			unset($this->rqv['error_return_url']);
			unset($this->rqv['success_return_url']);
			unset($this->rqv['ruleset']);

			if($ent && $this->ent != $ent)
			{
				$this->ent = $ent;
				$ent_list = explode('/', $ent);
				if(count($ent_list))
				{
					for($i = 0, $c = count($ent_list); $i < $c; $i += 2)
					{
						$key = $ent_list[$i];
						$val = trim($ent_list[$i + 1]);
						Context::set($key, $val);
						$this->rqv[$key] = $val;
					}
				}
			}

		}

	/**************************************************************/
	/*********** @private function					  ***********/

		function _get($args_list)
		{
			$retlst = array();

			for($i = 0, $c = count($args_list); $i < $c; $i++)
			{
				$key = $args_list[$i];
				$retlst->{$key} = $this->rqv[$key];
			}

			return (count($retlst) > 1) ? $retlst : current($retlst);
		}

		function _set($args_list)
		{
			for($i = 0, $c = count($args_list); $i < $c; $i += 2)
			{
				$key = $args_list[$i];
				$val = trim($args_list[$i + 1]);

				Context::set($key, $val);

				if(strlen($val)){
					$this->rqv[$key] = $val;
				}else if(isset($this->rqv[$key])){
					unset($this->rqv[$key]);
				}
			}
		}

	/**************************************************************/

		function get()
		{
			is_a($this, 'beluxeEntry') ?
				$self = &$this : $self = &beluxeEntry::getInstance()
			;

			return func_num_args()?$self->_get(func_get_args()):(object)$self->rqv;
		}

		function set()
		{
			is_a($this, 'beluxeEntry') ?
				$self = &$this : $self = &beluxeEntry::getInstance()
			;

			if(!func_num_args()) return;

			$self->_set(func_get_args());
		}

		function getUrl()
		{
			is_a($this, 'beluxeEntry') ?
				$self = &$this : $self = &beluxeEntry::getInstance()
			;

			$num_args  = func_num_args();
			$args_list = func_get_args();

			if($num_args)
			{
				$c = count($args_list);
				$i = (trim($args_list[0]) == '') ? 1 : 0;
				$e = $i ? array() : $self->rqv;

				// 값 셋팅
				for($i; $i < $c; $i += 2)
				{
					$key = $args_list[$i];
					$val = trim($args_list[$i + 1]);

					if(strlen($val)){
						$e[$key] = $val;
					}else if(isset($e[$key])){
						unset($e[$key]);
					}
				}

				$arg = array('');
				$try = array();

				// 보내기 형식에 맞게 분해
				foreach($e as $k=>$v)
				{
					if(!strlen($v)) continue;

					if(in_array($k, $self->not)){
						$arg[] = $k;
						$arg[] = $v;
					}else{
						$try[] = $k;
						$try[] = $v;
					}
				}

				if(count($try))
				{
					if(count($try) === 2 && $try[0] == 'document_srl'){
						$arg[] = $try[0];
						$arg[] = $try[1];
					}
					else{
						$arg[] = 'entry';
						$arg[] = implode('/', $try);
					}
				}

				// 분해된 값으로 url 추출
				$url = Context::getUrl(count($arg), $arg);
			}
			else $url = Context::getRequestUri();

			return $url;
		}
	}

?>
