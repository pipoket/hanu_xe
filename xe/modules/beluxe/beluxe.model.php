<?php

	class beluxeModel extends beluxe
	{
	/**************************************************************/
	/*********** @initialization						***********/

		function init()
		{
		}

	/**************************************************************/
	/*********** @private function						***********/

		/* @brief Set global datas */
		function _setGlobalList(&$data)
		{
			if(!$data) return;

			foreach($data as $key=>$val)
			{
				$item_srl = $val->document_srl;
				$GLOBALS['XE_BELUXE_LIST'][$item_srl] = $a_tmp[$key] = $val;
			}

			$data = $a_tmp;
		}


		/* @brief Get a module info */
		function _getModuleInfo($module_srl = 0)
		{
			// 정보가 있으면 리턴
			if($this->module_srl && $this->module_info->module_srl)
			{
				return $this->module_info;
			}

			// module model 객체 생성
			$oModuleModel = &getModel('module');

			if($module_srl)
			{
				return $oModuleModel->getModuleInfoByModuleSrl($module_srl);
			}else{
				$mid = Context::get('mid');
				if(!mid) $mid = Context::get('cur_mid');
				if(!mid) return;
				$site_info = Context::get('site_module_info');
				if($site_info) $site_srl = $site_info->site_srl;

				return $oModuleModel->getModuleInfoByMid($mid, $site_srl);
			}
		}

	/**************************************************************/
	/*********** @public function						***********/
	/**************************************************************/

		/* @brief Return tpl */
		function getBeluxeSkinTpl()
		{
			$args = Context::getRequestVars();
			if(!$args->mid)
			{
				$args->mid = $args->cur_mid;
				beluxeEntry::set('mid',$args->mid);
			}

			if($args->mid)
			{
				$m_info = $this->_getModuleInfo();

				if($args->class_act)
				{
					$class_act = $this->xml_info->action->{$args->class_act};
					$class_act->name = $args->class_act;

					// 개별 동작되고 타입이 있으면
					if($class_act->standalone && $class_act->type)
					{
						// 권한 체크
						$permission = $this->xml_info->permission->{$class_act->name};
						if($permission) $class_act->grant = $permission;

						switch ($class_act->grant)
						{
							case 'guest':
							break;
							case 'member':
								$logged_info = Context::get('logged_info');
								if(!$logged_info->member_srl || $logged_info->denied!='N' || ($logged_info->limit_date && $logged_info->limit_date < date('Ymdhis',time())))
									$tpl = Context::getLang('msg_not_permitted_act');
							break;
							default:
								if(!($this->grant->manager || $this->grant->is_admin || $this->grant->is_site_admin))
									$tpl = Context::getLang('msg_not_permitted_act');
						}

						// 문제 없으면 호출
						if(!$tpl)
						{
							// 초기화 후 호출
							$oCall = &getModule('beluxe', $class_act->type);
							$oCall->setAct($class_act->name);
							$oCall->setModuleInfo($m_info, $this->xml_info);
							$ret = call_user_func(array(&$oCall, $class_act->name));

							if($ret->error)
							{
								$tpl = Context::getLang($ret->message);
							}
						}
					}
					else $tpl = Context::getLang('msg_module_is_not_standalone');
				}

				// 문제 없으면 읽기
				if(!$tpl)
				{
					$skin = $m_info->skin ? $m_info->skin : 'default';
					$template_path = sprintf('%sskins/%s/',$this->module_path, $skin);

					$oTemplate = &TemplateHandler::getInstance();
					$tpl = str_replace("\t", ' ', $oTemplate->compile($template_path, 'template'));

					// 읽지못한 자바,css 직접 입력
					$tpl = preg_replace('/\/\/<!\[CDATA\[(.*?)\/\/\]\]>/is','$1', $tpl);
					$tpl = preg_replace_callback('/<!--(#)?Meta:([a-z0-9\_\/\.\@]+)(js|css)-->/is',
						create_function(
							'$matches',
							'if($matches[3] == \'js\') return \'<script type="text/javascript" src="\'.$matches[2].\'js"></script>\'; '.
							'else return \'<link rel="stylesheet" type="text/css" href="\'.$matches[2].\'css" />\';'
						),
						$tpl
					);

					// Changing user-defined language
					$oModuleController = &getController('module');
					$oModuleController->replaceDefinedLangCode($tpl);
				}
			}
			else $tpl = Context::getLang('msg_invalid_request');

			$this->add('tpl', $tpl);
		}

		/* @brief Get a list config */
		function getListConfig($module_srl)
		{
			if(!$module_srl) return;
			$cache_file = __XEFM_CACHE__.sprintf('%s.col.php', $module_srl);
			if(!file_exists($cache_file)){
				require_once(__XEFM_PATH__.'classes.cache.php');
				beluxeCache::columnConfigList($module_srl);
			}
			@include($cache_file);
			return is_array($list_config)?$list_config:array();
		}

		/* @brief Save the category in a cache file */
		function getCategoryFile($module_srl, $ext = '')
		{
			// Return if there is no information you need for creating a cache file
			if(!$module_srl) return;
			// Cache file's name
			$cache_file = __XEDM_CACHE__.sprintf('%s%s.php', $module_srl, $ext);
			if(!file_exists($cache_file)){
				require_once(__XEFM_PATH__.'classes.cache.php');
				beluxeCache::categoryList($module_srl);
			}
			return file_exists($cache_file)?$cache_file:'';
		}

		/* @brief Bringing the Categories list the specific module */
		function getCategoryList($module_srl, $ext = '')
		{
			if(!$module_srl) return;
			@include($this->getCategoryFile($module_srl, $ext));
			return ($menu&&is_array($menu->list))?$menu->list:array();
		}

		/* @brief Get a notice list */
		function getNoticeList($module_srl = 0)
		{
			$m_info = $this->_getModuleInfo($module_srl);
			if(!$m_info->module_srl) return;

			if($m_info->notice_category == 'Y') $category_srl = Context::get('category_srl');

			$oDocumentModel = &getModel('document');
			$args->module_srl = $m_info->module_srl;
			if($category_srl) $args->category_srl = $category_srl;

			return $oDocumentModel->getNoticeList($args);
		}

		/* @brief Get a best list */
		function getBestList($module_srl = 0)
		{
			$m_info = $this->_getModuleInfo($module_srl);
			if(!$m_info->module_srl) return;

			$module_srl = $m_info->module_srl;
			$s_voted_count = (int)$m_info->best_voted;
			$sort_index = $m_info->best_index;
			$list_count = (int)$m_info->best_count;
			$start_date =  date('Ymdhis', time() - (60*60*24* (int)$m_info->best_date));
			if($m_info->best_category == 'Y') $category_srl = Context::get('category_srl');

			// cache controll
			/* TODO 자주 바껴서 캐시까지 쓸 필요없을것 같다.
			$oCacheHandler = &CacheHandler::getInstance('object');
			if($oCacheHandler->isSupport()){
				$object_key = 'object:'.$module_srl.'_list_count:'.$list_count.'_sort_index:'.$bj->sort_index.'_start_date:'.$start_date;
				$cache_key = $oCacheHandler->getGroupKey('documentBestList', $object_key);
				$result = $oCacheHandler->get($cache_key);
			}
			*/

			if(!$result)
			{
				$args->module_srl = $module_srl;
				$args->list_count = $list_count;
				$args->sort_index = $sort_index;
				$args->s_voted_count = $s_voted_count?$s_voted_count:1;
				$args->start_date = $start_date;
				$args->order_type = 'desc';
				if($category_srl) $args->category_srl = $category_srl;

				$output = executeQueryArray('beluxe.getBestList', $args, $columnList);
				if(!$output->toBool()||!$output->data) return;

				foreach($output->data as $key => $val) {
					$document_srl = $val->document_srl;
					if(!$document_srl) continue;

					if(!$GLOBALS['XE_DOCUMENT_LIST'][$document_srl]) {
						$oDocument = null;
						$oDocument = new documentItem();
						$oDocument->setAttribute($val, false);
						$GLOBALS['XE_DOCUMENT_LIST'][$document_srl] = $oDocument;
					}
					$result->data[$document_srl] = $GLOBALS['XE_DOCUMENT_LIST'][$document_srl];
				}

				$oDocumentModel = &getModel('document');
				$oDocumentModel->setToAllDocumentExtraVars();

				foreach($result->data as $document_srl => $val) {
					$result->data[$document_srl] = $GLOBALS['XE_DOCUMENT_LIST'][$document_srl];
				}

				//insert in cache
				//if($oCacheHandler->isSupport()) $oCacheHandler->put($cache_key, $result);
			}

			return $result;
		}

		/* @brief Get a best list */
		function getBestCommentList($document_srl)
		{
			$m_info = $this->_getModuleInfo();
			if(!$m_info->module_srl) return;

			$module_srl = $m_info->module_srl;
			$s_voted_count = (int)$m_info->best_c_voted;
			$list_count = (int)$m_info->best_c_count;
			$start_date =  date('Ymdhis', time() - (60*60*24* (int)$m_info->best_c_date));

			// cache controll
			/* TODO 자주 바껴서 캐시까지 쓸 필요없을것 같다.
			$oCacheHandler = &CacheHandler::getInstance('object');
			if($oCacheHandler->isSupport()){
				$object_key = 'object:'.$document_srl.'_list_count:'.$list_count.'_start_date:'.$start_date;
				$cache_key = $oCacheHandler->getGroupKey('commentBestList_' . $module_srl, $object_key);
				$result = $oCacheHandler->get($cache_key);
			}
			*/

			if(!$result){
				$args->document_srl = $document_srl;
				$args->list_count = $list_count;
				$args->start_date = $start_date;
				$args->s_voted_count = $s_voted_count?$s_voted_count:1;
				$args->sort_index = 'voted_count';
				$args->order_type = 'desc';
				$output = executeQueryArray('beluxe.getBestCommentList', $args, $columnList);
				if(!$output->toBool()||!$output->data) return;

				require_once(_XE_PATH_.'modules/comment/comment.item.php');

				foreach($output->data as $key => $val) {
					if(!$val->comment_srl) continue;
					$oComment = null;
					$oComment = new commentItem();
					$oComment->setAttribute($val);
					$result->data[$val->comment_srl] = $oComment;
				}

				//insert in cache
				//if($oCacheHandler->isSupport()) $oCacheHandler->put($cache_key, $output);
			}

			return $result;
		}

	}

?>
