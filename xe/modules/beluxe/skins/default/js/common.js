/**
* Copyright 2011 All rights reserved by phiDel (www.foxb.kr, phidel@foxb.kr)
**/

jQuery.fn.scrollIntoView = function(b) {
	var el = this.get(0);
	if (el.scrollIntoView) {el.scrollIntoView(b ? b : true);}
	return this;
};

jQuery.fn.setEllipsis = function() {
	for(var i=0,c=this.length;i<c;i++){
		var $t = jQuery(this.get(i)),
			$f = jQuery('> :eq(0)', $t),
			$l = jQuery('> :eq(1)', $t),
			fw = $t.width(),
			lw = 0;
		if($l.length){
			if(!$l.html().trim().length) $l.remove(); else lw = $l.addClass('_last').outerWidth(true);
		}
		$f.css('width',(fw - lw - 5) + 'px').addClass('_first');
	}
};

jQuery(function($)
{
	// SNS에 글쓰기
	$('.scSns a').click(function(){
		var type = $(this).attr('rel'),
			$obj = $('.scView .hed li.title strong:eq(0)'),
			content = encodeURIComponent($obj.text().trim()),
			url = encodeURIComponent($obj.attr('title'));

		switch(type) {
			case 'me':
				loc = 'http://me2day.net/posts/new?new_post[body]=%22' + content + '%22%3A' + url;
				break;
			case 'fa':
				loc = 'http://www.facebook.com/share.php?t=' + content + '&u=' + url;
				break;
			case 'de':
				loc = 'http://www.delicious.com/save?v=5&noui&jump=close&url=' + url + '&title=' + content;
				break;
			default:
				loc = 'http://twitter.com/home?status=' + content + ' ' + url;
				break;
		}

		popopen(loc,'_pop_sns');
		return false;
	});

	$('div.scSubCate').each(function(){
		var $i = $(this),
			$p = $('.scView:eq(0),.scTable:eq(0)'),
			$u = $('ul', this),
			$k = $('a.scCaLock',this),
			lh = parseInt($('li:first', this).height()) + 30, //30 = margin-bottom
			uh = $u.height();
		if(!$p.length) return;
		if($p.length>1) $p = $($p.get(0));

		// 초반에 크기를 구하기 위해 visibility:hidden 사용했기에  숨기고 다시 켠다.
		$i.css({'display':'none','visibility':'visible'});

		if((lh*2)>uh){
			$('a.scCaPrev',this).css('display','none');
			$('a.scCaNext',this).css('display','none');
		}else{
			$u.css('margin-right','26px');
		}

		if($k){
			$k.click(function(){
				if($(this).hasClass('active')){
					$(this).removeClass('active');
					xDeleteCookie('scCaLock','/');
				}else{
					$(this).addClass('active');
					xSetCookie('scCaLock',true,null,'/');
				}
				return false;
			});

			if(xGetCookie('scCaLock')){$k.addClass('active');}
		}

		$('a.scCaPrev',this).click(function(){
			var t = parseInt($u.css("margin-top"));
			if(t < 0) $u.css('margin-top', (t + lh) + 'px');
			return false;
		});
		$('a.scCaNext',this).click(function(){
			var t = parseInt($u.css("margin-top")) - lh;
			if(t >= -uh) $u.css('margin-top', t + 'px');
			return false;
		});

		if($i.attr('rel') == 'autoHide'){
			$('thead tr:first th:not(.sort), ul.scFrm.hed', $p).mouseenter(function(event){
				var target = event.target,
					$isSp = $('> span.sort:eq(0)',target);
				if($isSp.length && (event.offsetX > $isSp.position().left)) return;
				$i.css({'top':$p.position().top+'px','width':$p.width()-10+'px'}).fadeIn();
			});
			$(document).mousemove(function(event){
				var target = event.target;
				if ($(target).parents().add(target).index($i) > -1) return;
				if(!$i.is(':hidden')&&$i.css('opacity')=='1'&&!$k.hasClass('active')) $i.fadeOut();
			});
		}

		// 일단 표시후 lock 아니면 천천히 사라지게...
		$i.css({'top':$p.position().top+'px','width':$p.width()-10+'px'}).fadeIn('fast');
		if(!xGetCookie('scCaLock')) $i.fadeOut('slow');
	});

	$('.scSideCate').each(function(){
		var $pr = $(this),
			$ca = $('.cateArea',$pr);
		$('.mubtn', $pr).click(function(){
				uw = $('ul.scFrm',$pr).outerWidth(true),
				bw = $('.mubtn',$pr).outerWidth(true);
			// right 이면 범위를 벗어나기 때문에 크기 계산
			if($ca.hasClass('right')) $ca.css({'margin-left':-(uw+bw)+'px','width':(uw+bw)+'px'});
			$('ul.scFrm', $pr).animate(
				{"width": "toggle", "opacity": "toggle"},
				"slow",
				function(){
					if($ca.hasClass('right') && $('ul.scFrm', $pr).is(':hidden')){
						$ca.css({'margin-left':-bw + 'px','width':bw + 'px'});
					}
				}
			);
			return false;
		});
		$('ul.scFrm li[rel]', $pr).each(function(){
			var rel = $(this).attr('rel'),
				$to = $('ul.scFrm li[rev='+rel+']', $pr);
			$('a b:first', this).click(function(){
				if($ca.hasClass('right')) $ca.css({'margin-left':'-1000px','width':'1000px'});
				$(this).html($to.is(':hidden')?'&rsaquo;':'&lsaquo;');
				$to.slideToggle();
				if($ca.hasClass('right')){
					uw = $('ul.scFrm',$pr).outerWidth(true),
					bw = $('.mubtn',$pr).outerWidth(true);
					$ca.css({'margin-left':-(uw+bw)+'px','width':(uw+bw)+'px'});
				}
				return false;
			}).css('color','black').html($to.css('display')=='none'?'&lsaquo;':'&rsaquo;');
		});
	});

	$('.scGall .scInfo[rel=autoHide]').each(function(){
		var $this = $(this),
			$parn = $(this).closest('.scItem');
		$parn.mouseenter(function(e){
			$this.slideDown();
		});
		$parn.mouseleave(function(e){
			$this.slideUp();
		});
	});

	$('.scClipboard').click(function(){
		var targ = $(this).attr('rel'), txt = $(this).attr(targ);

		/*
		if(window.clipboardData) {
			window.clipboardData.setData('Text', txt);
			alert('The text is copied to your clipboard...');
		}else{}
		*/

		prompt('press CTRL+C copy it to clipboard...',txt);
		return false;
	});

	$('a[href=#trackback_action][class=delete]').click(function()
	{
		var srl = $(this).attr('data-srl');
		if(srl==undefined || !srl) {
			alert('Please select the trackback.');
			return false;
		}
		if(!confirm('Do you want to delete the selected trackback?')) return false;
		exec_xml('beluxe', 'procBeluxeDeleteTrackback', {mid:current_mid,trackback_srl:srl}, completeCallModuleAction);
		return false;
	});

	$('.scShAdminBtns a[href=#siManageBtn]').click(function()
	{
		$('.scEllipsis').each(function(){
			var $of = $('._first', this);
			$of.css('width', ($of.width() - 30) + 'px');
		});

		$(this).hide();
		$('.scAdminActs').show('slow');
		$('.scCheck').show('slow');
		return false;
	});

	$('.scShAdminBtns .scAdminActs a').click(function()
	{
		var mode = $(this).attr('rel');
		switch(mode)
		{
			case 'manage':
				popopen(request_uri+'?module=document&act=dispDocumentManageDocument','manageDocument');
			break;
			case 'admin':
				location.href = current_url.setQuery('act','dispBeluxeAdminContent');
		};
		return false;
	});

	$('.scToggle').click(function(e){
		var targ = $(this).attr('rel'),
			is_not = $(this).attr('is_not');
		if(is_not!=undefined && $(e.target).is(is_not)) return true;
		$(targ).slideToggle();
		return false;
	});

	var write_mode = xGetCookie('write_mode');
	if(write_mode != undefined && write_mode)
	{
		xSetCookie('write_mode','',null,'/');
		write_mode = write_mode.split('/');

		if(write_mode[0] == 'comment')
			$('a.modalAnchor[href=#siModalWin][rel=' + write_mode[1] + '][rev=comment]', $('#comment li#comment_'+write_mode[2])).click();
		else if(write_mode[0] == 'insert' || write_mode[0] == 'update' || write_mode[0] == 'delete'){
			$('a.modalAnchor[href=#siModalWin][rel=' + write_mode[0] + '][rev!=comment]').click();
		}
	}

	$(window).load(function(){
		// ie7 이하에선 더미 넣어 계산해야 하는데 귀찮게하는 ie 확~마~! 안해 ㅡㅡ;
		if($.browser.msie!==true||($.browser.msie===true&&$.browser.version>7))	$('.scEllipsis').setEllipsis();
	});
});
