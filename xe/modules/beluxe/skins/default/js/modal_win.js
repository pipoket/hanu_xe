/* NHN (developers@xpressengine.com) Modal Window */

jQuery(function($){

	var ESC = 27;

	$.fn.xeModalWindow = function(){
		this
			.not('.xe-modal-window')
			.addClass('xe-modal-window')
			.each(function(){
				$( $(this).attr('href') ).addClass('x').hide();
			})
			.click(function(){
				var $this = $(this), $modal, $btnClose;

				// get and initialize modal window
				$modal = $( $this.attr('href') );
				if(!$modal.parent('body').length) {
					$btnClose = $('<button type="button" class="modalClose" title="Close this layer">X</button>');
					$btnClose.click(function(){ $modal.data('anchor').trigger('close.mw');});

					$modal
						.prepend('<span class="bg"></span>')
						.append('<!--[if IE 6]><iframe class="ie6"></iframe><![endif]-->')
						.find('>.fg')
							.prepend($btnClose)
							//.append($btnClose.clone(true))
						.end()
						.appendTo('body');
				}

				// set the related anchor
				$modal.data('anchor', $this);

				if($modal.data('state') == 'showing') {
					$this.trigger('close.mw');
				} else {
					$this.trigger('open.mw');
				}

				return false;
			})
			.bind('open.mw', function(){
				var $this = $(this), before_event, $modal, duration;

				// before event trigger
				before_event = $.Event('before-open.mw');
				$this.trigger(before_event);

				// is event canceled?
				if(before_event.isDefaultPrevented()) return false;

				// get modal window
				$modal = $( $this.attr('href') );

				// get duration
				duration = $this.data('duration') || 'fast';

				// set state : showing
				$modal.data('state', 'showing');

				// workaroud for IE6
				$('html,body').addClass('modalContainer');

				// after event trigger
				function after(){ $this.trigger('after-open.mw');};

				$(document).bind('keydown.mw', function(event){
					if(event.which == ESC) {
						$this.trigger('close.mw');
						return false;
					}
				});

				$modal
					.fadeIn(duration, after)
					.find('>.bg').height($(document).height()).end()
					.find('button.modalClose:first').focus();
			})
			.bind('close.mw', function(){
				var $this = $(this), before_event, $modal, duration;

				// before event trigger
				before_event = $.Event('before-close.mw');
				$this.trigger(before_event);

				// is event canceled?
				if(before_event.isDefaultPrevented()) return false;

				// get modal window
				$modal = $( $this.attr('href') );

				// get duration
				duration = $this.data('duration') || 'fast';

				// set state : hiding
				$modal.data('state', 'hiding');

				// workaroud for IE6
				$('html,body').removeClass('modalContainer');

				// after event trigger
				function after(){ $this.trigger('after-close.mw');};

				$modal.fadeOut(duration, after);
				$this.focus();
			});
	};

	$('a.modalAnchor').xeModalWindow();
	$('div.modal').addClass('x').hide();

	$.fn.writeSkinTpl = function(params) {
		var $this = $(this);

		exec_xml(
			'beluxe',
			'getBeluxeSkinTpl',
			params,
			function(ret){
				var tpl = ret.tpl.replace(/(\n|\r)/g, ' ');

				// 에디터의 파일첨부 관련 자바 오류로 비활성 되는거 수정하기 (onload 가 아니라 init 전에 발생해서 뒤로옮김)
				var patt = /<script[^<]+editorUploadInit[^>]+\/script>/g;
				if(patt.test(tpl))
				{
					var scUp = tpl.match(patt);
					tpl = tpl.replace(patt, '') + ' ' + scUp;
				}

				// load_js_plugin 정보 없어서 수동입력 필요.
				var patt = /[^jquery\.ui\.datepicker]*<script[^<]+\.datepicker[^>]+\/script>+[^jquery\.ui\.datepicker]/g;
				if(patt.test(tpl)){
					tpl = '<link rel="stylesheet" href="/common/js/plugins/ui/jquery-ui.css" type="text/css"></link> \
							<script type="text/javascript" src="/common/js/plugins/ui/jquery-ui.min.js"></script> \
							<script type="text/javascript" src="/common/js/plugins/ui/jquery.ui.datepicker-ko.js"></script> '
						+ tpl;
				}
				var patt = /[^ui\.krzip]*<[^<]+onclick[^<]+doSearchKrZip[^>]+>+[^ui\.krzip]/g;
				if(patt.test(tpl)){
					tpl = tpl + ' <script type="text/javascript" src="/common/js/plugins/ui.krzip/krzip_search.js"></script>';
				}

				// 메세지가 있으면 복사본 만듬어 넣기
				if($('.message').length) $this.append($('.message:eq(0)').clone(true).css({'position':'absolute','top':'10px','left':'10px'}));
				$(tpl).appendTo($('fieldset:eq(0)', $this));
				$('a.btnModalClose', $this).click(function(){ $this.data('anchor').trigger('close.mw'); return false; });
				$('input:text:eq(0)', $this).focus();
			},
			['error','message','tpl']
		);
	};

	$.fn.writeSkinIframe = function(url) {
		var $this = $(this);

		if($('.message').length) $this.append($('.message:eq(0)').clone(true).css({'position':'absolute','top':'10px','left':'10px'}));
		if($('.wfsr').length) $this.append($('<div class="message info"></div>').html('<p>' + waiting_message + '</p>').css({'position':'absolute','top':($('.message').length?'30px':'10px'),'left':'10px'}));

		if(!$('#siIframe', $this).length)
		{
			$('<iframe id="siIframe" allowTransparency="true" frameborder="0" src="'+url+'" scrolling="no">')
				.appendTo($('fieldset:eq(0)', $this));
		}else{
			$('#siIframe', $this).attr('src', url);
			window.frames["siIframe"].location.href = url;
		}
	};

	$('a.modalAnchor:[rel=insert],[rel=update],[rel=delete],[rel=reply]').bind('before-open.mw', function(e){
		var $modalWin = $($(this).attr('href')),
			mode = $(this).attr('rel'),
			type = $(this).attr('rev'),
			is_c = (type != undefined && type == 'comment'),
			is_iframe = $('fieldset:eq(0)', $modalWin).attr('rel') == 'iframe' || $.browser.msie===true,
			url = current_url;

		// 로딩중 안보이게 처리
		$('div.fg', $modalWin).css('left','-9000px');

		var params = is_iframe?url.getEntryQuerys('document_srl'):url.getEntryQuerys();
		params['mid'] = current_mid;
		params[is_iframe?'act':'class_act'] = 'dispBoard' + (mode=='delete'?'Delete':'Write') + (is_c?'Comment':'');
		if(mode=='insert'||mode=='reply') params[(is_c?'comment_srl':'document_srl')] = '';

		// 댓글일때
		if(mode!='insert') {
			if(is_c) {
				var srl = $(this).closest('[data-com-srl]').attr('data-com-srl');
				params[mode=='reply'?'parent_srl':'comment_srl'] = srl;
			}

			if(!params['document_srl']){
				var srl = $(this).closest('[data-doc-srl]').attr('data-doc-srl');
				if(srl == undefined || !srl) {
					alert('Can not find the document_srl.');
					return false;
				}
				params['document_srl'] = srl;
			}
		}

		// IE 버그, 이벤트 해제가 안됨, 방법이 생길때 까지 아이프레임
		if(is_iframe){
			params['is_iframe'] = '1';
			$modalWin.writeSkinIframe(url.setEntryQuerys(params));
		}else{
			params['tpl_name'] = (is_c?'c.':'') + ((mode=='update'||mode=='reply')?'insert':mode);
			$modalWin.writeSkinTpl(params);
		}

	}).bind('after-close.mw', function(e){
		var $modalWin = $($(this).attr('href')),
			is_iframe = $('fieldset:eq(0)', $modalWin).attr('rel') == 'iframe' || $.browser.msie===true;

		// 초기화 (순서 중요)
		$('.message', $modalWin).remove();
		// IE 버그, 이벤트 해제가 안됨, 방법이 생길때 까지 아이프레임, IE 처리안함
		if(!is_iframe) $('fieldset:eq(0) > *', $modalWin).remove();
	});

});
