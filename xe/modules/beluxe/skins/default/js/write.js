/**
* Copyright 2011 All rights reserved by phiDel (www.foxb.kr, phidel@foxb.kr)
**/

jQuery(function($)
{

	$('.scWrite select.scWcateList').change(function(){
		var val = $(this).val(),
			key = $(this).data('key'),
			$od = $('select.scWcateList[rev='+key+']'),
			$os =$('select.scWcateList[rev='+val+']');
		$(this).data('key', val);

		$('input:hidden[name=category_srl]').val(val);

		$('select.scWcateList[rev='+$od.data('key')+']').hide('slow');
		$od.hide('slow');
		$os.show('slow').change();
	});

	$('input:hidden[name=category_srl]').each(function(){
		var val = $(this).val();
		if(val!=undefined && val > 0){
			var i=0, $select = null;
			while ($select = $('.scWrite select.scWcateList option[value='+val+']').closest('select'))
			{
				$select.data('key', val);
				val = $select.val(val).show().attr('rev');
				if(val == undefined || !val || (i++ > 9)) break;
			}
		}else{
			$('.scWrite select.scWcateList:eq(0)').change();
		}
	});

	$('#insert_filelink a[href=#insert_filelink]').click(function(){
		var $input = $(this).closest('#insert_filelink').find('> input'),
			val = $input.val(),
			seq = $(this).attr('rel'),
			srl = $(this).attr('rev');

		if(val == undefined || !val){
			alert('Please enter the file url.');
			$input.focus();
			return false;
		}

		exec_xml(
			'Beluxe',
			'procBeluxeInsertFileLink',
			{
				'mid':current_mid,
				'sequence_srl':seq,
				'document_srl':srl,
				'filelink_url':val
			},
			function(ret){
				var seq = ret.sequence_srl;
				reloadFileList(uploaderSettings[seq]);
			},
			['error','message','sequence_srl']
		);

		return false;
	});

	// TODO ruleset 확장 변수 체크 코어에서 고칠때까지 임시 조치
	$('form .extraKeys:eq(0)').each(function(){
		var $form = $(this).closest('form');
		$form.submit(function(){
			var $li = $('form .extraKeys li.scWli');

			if($li.length) {
				for(var i=0, c=$li.length;i<c;i++){
					var $put = $('input[name],select[name],textarea[name]',$li.get(i)),
						req = $('.required', $li.get(i)).text();

					if($put.length){
						var name = $put.attr('name'),
							oooo = $put.get(0);

						if(!$put.is('[type=radio]') && $put.length > 1 && name.indexOf('[') == -1) $put.attr('name', name+'[]');

						if($put.is('[type=checkbox]'))$put = $('input:checked',$li.get(i));
						if($put.is('[type=radio]')) $put = $('input:checked',$li.get(i));

						var vals = new Array();
						for(var k=0, u=$put.length;k<u;k++){
							var vv = $($put.get(k)).val();
							if(typeof(vv)=='string'&&vv) vals[k] = vv;
						}

						var val = vals.join('|@|');
						if(req && !val){
							alert(name + ': required');
							oooo.focus();
							return false;
						}
					}
				}
			}
		});
	});
});
