/**
 * jQuery.timers - Timer abstractions for jQuery
 * Written by Blair Mitchelmore (blair DOT mitchelmore AT gmail DOT com)
 * Licensed under the WTFPL (http://sam.zoy.org/wtfpl/).
 * Date: 2009/10/16
 *
 * @author Blair Mitchelmore
 * @version 1.2
 *
 **/

jQuery.fn.extend({
	everyTime: function(interval, label, fn, times) {
		return this.each(function() {
			jQuery.timer.add(this, interval, label, fn, times);
		});
	},
	oneTime: function(interval, label, fn) {
		return this.each(function() {
			jQuery.timer.add(this, interval, label, fn, 1);
		});
	},
	stopTime: function(label, fn) {
		return this.each(function() {
			jQuery.timer.remove(this, label, fn);
		});
	}
});

jQuery.extend({
	timer: {
		global: [],
		guid: 1,
		dataKey: "jQuery.timer",
		regex: /^([0-9]+(?:\.[0-9]*)?)\s*(.*s)?$/,
		powers: {
			// Yeah this is major overkill...
			'ms': 1,
			'cs': 10,
			'ds': 100,
			's': 1000,
			'das': 10000,
			'hs': 100000,
			'ks': 1000000
		},
		timeParse: function(value) {
			if (value == undefined || value == null)
				return null;
			var result = this.regex.exec(jQuery.trim(value.toString()));
			if (result[2]) {
				var num = parseFloat(result[1]);
				var mult = this.powers[result[2]] || 1;
				return num * mult;
			} else {
				return value;
			}
		},
		add: function(element, interval, label, fn, times) {
			var counter = 0;

			if (jQuery.isFunction(label)) {
				if (!times)
					times = fn;
				fn = label;
				label = interval;
			}

			interval = jQuery.timer.timeParse(interval);

			if (typeof interval != 'number' || isNaN(interval) || interval < 0)
				return;

			if (typeof times != 'number' || isNaN(times) || times < 0)
				times = 0;

			times = times || 0;

			var timers = jQuery.data(element, this.dataKey) || jQuery.data(element, this.dataKey, {});

			if (!timers[label])
				timers[label] = {};

			fn.timerID = fn.timerID || this.guid++;

			var handler = function() {
				if ((++counter > times && times !== 0) || fn.call(element, counter) === false)
					jQuery.timer.remove(element, label, fn);
			};

			handler.timerID = fn.timerID;

			if (!timers[label][fn.timerID])
				timers[label][fn.timerID] = window.setInterval(handler,interval);

			this.global.push( element );

		},
		remove: function(element, label, fn) {
			var timers = jQuery.data(element, this.dataKey), ret;

			if ( timers ) {

				if (!label) {
					for ( label in timers )
						this.remove(element, label, fn);
				} else if ( timers[label] ) {
					if ( fn ) {
						if ( fn.timerID ) {
							window.clearInterval(timers[label][fn.timerID]);
							delete timers[label][fn.timerID];
						}
					} else {
						for ( var fn in timers[label] ) {
							window.clearInterval(timers[label][fn]);
							delete timers[label][fn];
						}
					}

					for ( ret in timers[label] ) break;
					if ( !ret ) {
						ret = null;
						delete timers[label];
					}
				}

				for ( ret in timers ) break;
				if ( !ret ) jQuery.removeData(element, this.dataKey);
			}
		}
	}
});

jQuery(window).bind("unload", function() {
	jQuery.each(jQuery.timer.global, function(index, item) {
		jQuery.timer.remove(item);
	});
});

jQuery(function($)
{
	$('#siModalWin', (parent ? parent.document : document)).each(function()
	{
		var $modal = $(this),
			$window = $(parent ? parent : window);

		if($('#siIframe', $modal).length){

			$modal.everyTime(500, 'template',function(i){
				var $this = $('#siIframe', $modal),
					$parent = $('div.fg', $modal),
					chkh = $window.height() - 100;

				if($modal.is(':hidden') || !$this.length) {
					$modal.stopTime('template');
					if(!$this.length) return;
					$this.css('height','');
					$parent.css({'overflow-y':'hidden','height':''});
					return;
				}

				$this.height($('#siTemplate').outerHeight(true) + 10);

				if(chkh < $this.height())
				{
					$parent.css({'overflow-y':'scroll','height':chkh+'px'});
				}else if(chkh > $this.height())
				{
					$parent.css({'overflow-y':'hidden','height':''});
				}

				$parent.css('top', (($window.height() - $parent.outerHeight()) / 2) - 20 + 'px');
				$parent.css('left', (($window.width() - $parent.outerWidth()) / 2) + 'px');
			});

			$('a.btnModalClose').click(function(){
				$('button.modalClose:eq(0)', parent.document).click();
			});

			$('.message', $modal).remove();

		}else{

			$modal.everyTime(500, 'template',function(i){
				var $this = $('#siTemplate', $modal),
					$parent = $('div.fg', $modal),
					chkh = $window.height() - 100;

				if($modal.is(':hidden') || !$this.length){
					$modal.stopTime('template');
					return;
				}

				var ffhbug = $('.scLtitle', $this).outerHeight(true) + $('.scWrite', $this).outerHeight(true);

				if(chkh < $this.height())
				{
					$this.css({'overflow-y':'scroll','height':chkh+'px','padding-right':'3px'});
				}else if((chkh > ffhbug)||(chkh > $parent.height()))
				{
					$this.css({'overflow-y':'hidden','height':'','padding-right':''});
				}

				$parent.css('top', (($window.height() - $parent.outerHeight()) / 2) - 20 + 'px');
				$parent.css('left', (($window.width() - $parent.outerWidth()) / 2) + 'px');
			});

		};
	});
});
