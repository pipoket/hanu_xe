<?php
	class beluxeAdminView extends beluxe
	{

	/******************************************************************/
	/*********** @initialization							***********/

		function init()
		{

			// 관리자용 언어팩은 따로 읽기
			Context::loadLang($this->module_path.'lang/admin');

			// module_srl이 있으면 미리 체크하여 존재하는 모듈이면 module_info 세팅
			$module_srl = Context::get('module_srl');
			if(!$module_srl && $this->module_srl)
			{
				$module_srl = $this->module_srl;
				Context::set('module_srl', $module_srl);
			}

			$oModuleModel = &getModel('module');

			// module_srl이 넘어오면 해당 모듈의 정보를 미리 구해 놓음
			if($module_srl)
			{
				// module model 객체 생성
				$module_info = $oModuleModel->getModuleInfoByModuleSrl($module_srl);
				if(!$module_info)
				{
					Context::set('module_srl','');
					$this->act = 'list';
				}
				else
				{
					ModuleModel::syncModuleToSite($module_info);
					$this->module_info = $module_info;
					if(!$this->module_info->skin) $this->module_info->skin = 'default';
					Context::set('module_info', $this->module_info);
				}
			}

			$this->module_srl = $module_info->module_srl;

			// 정렬 옵션을 세팅
			$order = explode(',', __XEFM_ORDER__);
			foreach($order as $key) $order_target[$key] = Context::getLang($key);
			$order_target['list_order'] = Context::getLang('document_srl');
			$order_target['update_order'] = Context::getLang('last_update');
			Context::set('order_target', $order_target);

			// 모듈 카테고리 목록을 구함
			$module_category = $oModuleModel->getModuleCategories();
			Context::set('module_category', $module_category);

			// 관리용 템플릿 경로 지정
			$this->setTemplatePath($this->module_path.'tpl/');
		}

	/******************************************************************/
	/*********** @private function							***********/

	/******************************************************************/
	/*********** @public function							***********/

		/* @brief Display a content */
		function dispBeluxeAdminContent()
		{
			$this->setTemplateFile('index');
		}

		/* @brief Display an insert info of beluxe */
		function dispBeluxeAdminInsert()
		{
			// 스킨 목록을 구해옴
			$oModuleModel = &getModel('module');
			$skin_list = $oModuleModel->getSkins($this->module_path);
			Context::set('skin_list',$skin_list);

			$mskin_list = $oModuleModel->getSkins($this->module_path, 'm.skins');
			Context::set('mskin_list', $mskin_list);

			// 레이아웃 목록을 구해옴
			$oLayoutModel = &getModel('layout');
			$layout_list = $oLayoutModel->getLayoutList();
			Context::set('layout_list', $layout_list);

			$mobile_layout_list = $oLayoutModel->getLayoutList(0,'M');
			Context::set('mlayout_list', $mobile_layout_list);

			// get document status list
			$oDocumentModel = &getModel('document');
			$documentStatusList = $oDocumentModel->getStatusNameList();
			Context::set('document_status_list', $documentStatusList);

			$this->setTemplateFile('insert');
		}

		/* @brief Display a info of beluxe */
		function dispBeluxeAdminModuleInfo()
		{
			$this->dispBeluxeAdminInsert();
		}

		/* @brief Display a list of beluxe */
		function dispBeluxeAdminList()
		{
			$args->s_module_category_srl = Context::get('module_category_srl');
			$args->page = Context::get('page');
			$output = executeQueryArray(__XEFM_NAME__.'.getBeluxeList', $args);
			ModuleModel::syncModuleToSite($output->data);

			Context::set('total_count', $output->total_count);
			Context::set('total_page', $output->total_page);
			Context::set('page', $output->page);
			Context::set('this_module_list', $output->data);
			Context::set('page_navigation', $output->page_navigation);

			$this->setTemplateFile('list');
		}

		/* @brief Display a category info */
		function dispBeluxeAdminCategoryInfo()
		{
			$filename = sprintf(__XEDM_CACHE__.'%s.php', $this->module_srl);
			// If the target file to the cache file regeneration category
			if(!file_exists($filename)) {
				require_once(__XEFM_PATH__.'classes.cache.php');
				beluxeCache::categoryList($module_srl);
			}

			@include($filename);
			Context::set('menu', $menu);

			// Get a list of member groups
			$oMemberModel = &getModel('member');
			$group_list = $oMemberModel->getGroups($module_info->site_srl);
			Context::set('group_list', $group_list);

			$filename = sprintf('%sskins/%s/type.xml',$this->module_path, $this->module_info->skin);
			$parser = XmlParser::loadXmlFile($filename);
			$type_list = array();
			if(count($parser->type->item))
				foreach ($parser->type->item as $val)
				{
					unset($attrs);
					$attrs->title = $val->value->body;
					// 분류 관리에서 사용하기 위해 index, type, list, page 순서대로 미리 입력후 이외것들은 제외
					$attrs->attrs = array('sort_index'=>'', 'order_type'=>'', 'list_count'=>'', 'page_count'=>'');
					foreach ($val->attrs as $kk=>$vv)
					{
						if(isset($attrs->attrs[$kk])) $attrs->attrs[$kk] = $vv;
					}
					$type_list[$val->attrs->name] = $attrs;
				}
			Context::set('type_list', $type_list);

			if(!$this->module_info->category_default_title) $this->module_info->category_default_title = Context::getLang('category');

			Context::loadJavascriptPlugin('ui.colorpicker');

			$this->setTemplateFile('category');
		}


		/* @brief Display a skin info */
		function dispBeluxeAdminSkinInfo()
		{
			$oModuleAdminModel = &getAdminModel('module');
			$skin_content = $oModuleAdminModel->getModuleSkinHTML($this->module_srl);
			if(!Context::get('skin_info')) $this->stop('msg_not_skin_info');

			Context::set('skin_content', $skin_content);

			$this->setTemplateFile('skin');
		}

		/* @brief Display a grant info */
		function dispBeluxeAdminGrantInfo()
		{
			$oModuleAdminModel = &getAdminModel('module');
			$grant_content = $oModuleAdminModel->getModuleGrantHTML($this->module_srl, $this->xml_info->grant);
			Context::set('grant_content', $grant_content);

			$this->setTemplateFile('grant');
		}

		/* @brief Setting a addition */
		function dispBeluxeAdminAdditionSetting()
		{
			// content는 다른 모듈에서 call by reference로 받아오기에 미리 변수 선언만 해 놓음
			$content = '';

			// 추가 설정을 위한 트리거 호출
			// 게시판 모듈이지만 차후 다른 모듈에서의 사용도 고려하여 trigger 이름을 공용으로 사용할 수 있도록 하였음
			$output = ModuleHandler::triggerCall('module.dispAdditionSetup', 'before', $content);
			$output = ModuleHandler::triggerCall('module.dispAdditionSetup', 'after', $content);
			Context::set('setup_content', $content);

			// 템플릿 파일 지정
			$this->setTemplateFile('addition');
		}

		/* @brief Setting a list column */
		function dispBeluxeAdminColumnSetting()
		{
			// 대상 항목을 구함
			$oThisModel = &getModel(__XEFM_NAME__);
			$list_config = $oThisModel->getListConfig($this->module_srl);

			// 설정 항목 추출 (설정항목이 없을 경우 기본 값을 세팅)
			Context::set('list_config', $list_config);

			$security = new Security();
			$security->encodeHTML('list_config..name');

			Context::loadJavascriptPlugin('ui.colorpicker');

			$this->setTemplateFile('column');
		}

		/* @brief Setting a extra vars */
		function dispBeluxeAdminExtraKeys()
		{
			$oDocumentModel = &getModel('document');
			// Bringing existing extra_keys
			$extra_keys = $oDocumentModel->getExtraKeys($this->module_srl);
			Context::set('extra_keys', $extra_keys);

			$this->setTemplateFile('extra.keys');
		}

	/******************************************************************/

	}

?>
