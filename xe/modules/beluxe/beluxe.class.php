<?php

	define('__XEFM_NAME__', 'beluxe');
	define('__XEFM_PATH__', './modules/'.__XEFM_NAME__.'/');
	define('__XEFM_CACHE__', './files/cache/'.__XEFM_NAME__.'/');
	define('__XEDM_CACHE__', './files/cache/document_category/');
	define('__XEFM_ORDER__', 'list_order,update_order,regdate,voted_count,readed_count,comment_count,title');

	require_once(__XEFM_PATH__.'classes.item.php');
	require_once(__XEFM_PATH__.'classes.entry.php');

	class beluxe extends ModuleObject
	{

		/* @brief Install the module */
		function moduleInstall()
		{
			return new Object();
		}

		/* @brief Check the module */
		function checkUpdate()
		{
			return false;
		}

		/* @brief Updaet the module */
		function moduleUpdate()
		{
			return new Object(0, 'success_updated');
		}

		/* @brief Uninstall the module */
		function moduleUninstall()
		{
			$this->recompileCache();
			return new Object();
		}

		/* @brief create the cache */
		function recompileCache()
		{
			if(is_dir(__XEFM_CACHE__)) FileHandler::removeDir(__XEFM_CACHE__);
			if(is_dir(__XEDM_CACHE__)){
				$directory = dir(__XEDM_CACHE__);
				while($entry = $directory->read()) {
					if ($entry != '.' && $entry != '..' && !is_dir(__XEDM_CACHE__.$entry)) {
						if (preg_match ('/'.$module_srl.'\..+/', $entry)) @unlink(__XEDM_CACHE__.$entry);
					}
				}
				$directory->close();
			}
		}

	}

?>
